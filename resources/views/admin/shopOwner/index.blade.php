@extends('layouts.app')

@section('content')
@include('layouts.headers.header',
array(
'class'=>'info',
'title'=>__("Shop Owner"),'description'=>'',
'icon'=>'fas fa-home',
'breadcrumb'=>array([
'text'=>__('Shop Owner List')
])))
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header mb-3">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Shop Owner') }}</h3>
                        </div>

                    </div>
                </div>

                <div class="col-12">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>
                <div class="table-responsive py-4">
                    <table id="dataTable" class="table table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Email')}}</th>
                                <th>{{__('Phone No')}}</th>
                                <th>{{__('Provider')}}</th>
                                <th>{{__('Point to withdraw')}}</th>

                                <th>{{__('Status')}}</th>
                                <th>{{__('Action')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($owner as $ss)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ $ss->name}}
                                    @if ($ss->is_daesea)
                                    <span class="badge  badge-info m-1">{{__('Daesea')}}</span>
                                    @endif
                                </td>
                                <td><a href="mailto:{{$ss->email}}">{{$ss->email}}</a></td>
                                <td><a href="tel:{{$ss->phone_no}}">{{$ss->phone_no}}</a></td>
                                 <td>

                                     @if ($ss->provider == "LOCAL")
                                     <span class="badge  badge-primary m-1">{{__('Local')}}</span>
                                     @elseif($ss->provider == "KAKAO")
                                     <span class="badge  badge-info  m-1">{{__('KAKAO')}}</span>
                                     @else
                                     <span class="badge  badge-success  m-1">{{__('NEVER')}}</span>

                                     @endif
                                 </td>
                                <td>{{$ss->balance}}</td>
                                <td>
                                    @if ($ss->status)
                                    <span class="badge  badge-success m-1">{{__('Active')}}</span>
                                    @else
                                    <span class="badge  badge-warning  m-1">{{__('Block')}}</span>

                                    @endif
                                </td>
                                <td class="d-flex">




                                    <a href="{{ route('shopowner.show', ['id'=>$ss->id]) }}"
                                        class="btn btn-primary btn-sm btn-outline"> {{__('View')}}</a>
                                    <form action="{{ route('shopowner.statusChange', $ss) }}" method="post">
                                        @csrf

                                        <button type="button"
                                            class="btn btn-sm btn-outline-{{$ss->status ?'danger' :'primary'}} btn-icon m-1"
                                            onclick="confirm('{{ __("Are you sure you want to change status of this user?") }}') ? this.parentElement.submit() : ''">
                                            <span class="ul-btn__icon">
                                                @if ($ss->status)
                                                <i class="fas fa-ban"></i>
                                                @else
                                                <i class="fas fa-shield-alt"></i>
                                                @endif
                                            </span>
                                        </button>
                                    </form>

                                    <form action="{{ route('shopowner.daesea', $ss) }}" method="post">
                                        @csrf

                                        <button type="button"
                                            class="btn btn-sm btn-outline-{{$ss->is_daesea ?'warning' :'primary'}} btn-icon m-1"
                                            onclick="confirm('{{ __("Are you sure you want to change status of this user?") }}') ? this.parentElement.submit() : ''">

                                            @if ($ss->is_daesea)
                                            {{__('Disable Daesea')}}
                                            @else

                                            {{__('Enable Daesea')}}
                                            @endif

                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection