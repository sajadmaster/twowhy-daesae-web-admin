@extends('layouts.app')

@section('content')
@include('layouts.headers.header',
array(
'class'=>'info',
'title'=>__("announces"),'description'=>'',
'icon'=>'fas fa-home',
'breadcrumb'=>array([
'text'=>__('announce List')
])))
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header mb-3">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('announce') }}</h3>
                        </div>

                        <div class="col-4 text-right">
                            <a href="{{ route('announce.create') }}" class="btn btn-sm btn-primary">{{ __('Add announce') }}</a>
                        </div>

                    </div>
                </div>

                <div class="col-12">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
    <div class="row mt-5 mb-5">
        @foreach ($announces as $announce)

        <div class="col-sm-12 col-md-4">
            <div class="card shadow">
                <!-- Card image -->
                <img class="card-img-top" height="250" src="{{ asset('upload') .'/'.$announce->icon}}">
                <!-- List group -->
                <ul class="list-group list-group-flush">
                    <!--<li class="list-group-item">View :- {{$announce->views}}</li>-->

                    <li class="list-group-item">
                        @foreach ($announce->tag as $tag)

                        <span class="badge   badge-warning  m-1">{{$tag}}</span>
                        @endforeach
                    </li>
                </ul>
                <!-- Card body -->
                <div class="card-body">
                    <h3 class="card-title mb-3">{{$announce->title}}</h3>
                    <div class="card-text mb-4">
                        
                        {!! $announce->detail !!}

                    </div>
                    <div class="d-flex">

                        <a class="btn btn-primary" href="{{ route('announce.edit', $announce->id) }}">
                            {{__('Edit')}}
                        </a>
                        <form action="{{ route('announce.destroy', $announce) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="button" class="btn btn-danger"
                                onclick="confirm('{{ __("Are you sure you want to delete this?") }}') ? this.parentElement.submit() : ''">
                                {{__('Delete')}}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @endforeach
    </div>
</div>
@endsection