@extends('layouts.app')

@section('content')
@include('layouts.headers.header',
array(
'class'=>'info',
'title'=>__("Point Package"),'description'=>'',
'icon'=>'fas fa-home',
'breadcrumb'=>array([
'text'=>__('Point Package'),
'text'=>__('Edit Point Package'),
])))
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header ">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Edit Point Package Detail') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('point-package.index') }}"
                                class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                        </div>
                    </div>
                </div>


                <div class="card-body">

                    <form enctype="multipart/form-data"
                        action="{{ route("point-package.update", [$pointPackage->id]) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="validationDefault01">{{__('Name:')}}</label>
                                    <input type="text" name="name" value="{{ old('name',$pointPackage->name) }}"
                                        class="form-control  @error('name') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Name')}}" autofocus required>

                                    @error('name')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('Detail:')}}</label>
                                    <input type="text" name="detail" value="{{ old('detail',$pointPackage->detail) }}"
                                        class="form-control  @error('detail') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Detail')}}" autofocus required>

                                    @error('detail')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="validationDefault01">{{__('Price:')}}</label>
                                    <input type="number" name="price" value="{{ old('price',$pointPackage->price) }}"
                                        class="form-control  @error('price') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Price')}}" autofocus required>

                                    @error('price')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="validationDefault01">{{__('Point:')}}</label>
                                    <input type="text" name="point" value="{{ old('point',$pointPackage->point) }}"
                                        class="form-control  @error('point') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Point')}}" autofocus required>

                                    @error('point')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>


<div class="col-md-6 mb-3">
    <div class="form-group d-flex">
        <label class="form-control-label" for="validationDefault01">{{__('Status:')}}</label>
        <label class="custom-toggle custom-toggle-primary ml-2">
            <input {{$pointPackage->status ? 'checked' : ''}} type="checkbox" value="1" name="status">
            <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
        </label>
        @error('status')
        <div class="invalid-div">{{ $message }}</div>
        @enderror


    </div>
</div>
                            


                        </div>


                        <button class="btn btn-primary" type="submit">{{__('Submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection