@extends('layouts.app')

@section('content')
@include('layouts.headers.header',
array(
'class'=>'info',
'title'=>__("Customer"),'description'=>'',
'icon'=>'fas fa-home',
'breadcrumb'=>array([
'text'=>__('Customer List')
])))
<div class="container-fluid mt--7">

    <div class="row">
        <div class="col-12 mb-5">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <form action="{{ route('report.user' ) }}" method="POST" class="d-flex">
                        @csrf

                        <div class="form-group mr-5">
                            <label>{{ __('Start Date') }}</label>
                            <input type="date" name="start_date"
                                class="form-control @error('start_date') is-invalid @enderror" required
                                value="{{ old('start_date') }}">
                        </div>
                        <div class="form-group mr-5">
                            <label>{{ __('End Date') }}</label>
                            <input type="date" name="end_date"
                                class="form-control @error('end_date') is-invalid @enderror" required
                                value="{{ old('end_date') }}">
                        </div>
                        <button style="height: 40px;" class="btn btn-success mt-4"
                            type="submit">{{ __('Search') }}</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{__('Total Users')}}</h5>
                            <span class="h2 font-weight-bold mb-0">{{count($data)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fas fa-chart-bar"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{__('Total Block User')}}</h5>
                            <span
                                class="h2 font-weight-bold mb-0">{{ collect($data)->where('status', 0)->count()}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fas fa-chart-bar"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">{{__('Total Active User')}}</h5>
                            <span
                                class="h2 font-weight-bold mb-0">{{ collect($data)->where('status', 1)->count()}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                <i class="fas fa-chart-bar"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col">
            <div class="card shadow">
                <div class="card-header mb-3">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Customer') }}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>
                <div class="table-responsive py-4">
                    <table id="dataTable" class="table table-flush">

                        <thead class="thead-light">
                            <tr>
                                <th>
                                    {{__(' #')}}
                                </th>
                                <th>
                                    {{__('Name')}}
                                </th>
                                <th>
                                    {{__('Email')}}
                                </th>
                                <th>
                                    {{__('Phone No')}}
                                </th>
                                <th>
                                    {{__('Status')}}
                                </th>

                                <th>
                                    {{__('Image')}}
                                </th>
                                <th>
                                    {{__('Register')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ $item->name}}</td>
                                <td>{{ $item->email}}</td>
                                <td>{{ $item->phone_no}}</td>

                                <td>
                                    @if ($item->status == 0)
                                    <span class="badge   badge-danger m-1">{{__('Block')}}</span>
                                    @elseif($item->status == 1)
                                    <span class="badge   badge-success  m-1">{{__('Active')}}</span>

                                    @endif
                                </td>

                                <td>
                                    <img class=" img-fluid" src="{{ asset('upload') .'/'.$item->image}}" alt=""
                                        height="50" width="50">
                                </td>

                                <td class="d-flex">
                                    {{$item->created_at}}
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection