<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
            aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('argon') }}/img/brand/blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-1-800x800.jpg">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>

                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>

        <div class="collapse navbar-collapse" id="sidenav-collapse-main">

            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                            data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                            aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>

            <ul class="navbar-nav">
                <h6 class="navbar-heading text-muted" style="padding: 0rem 1.5rem;">{{__('Navigation')}}</h6>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">
                        <i class="fas fa-tachometer-alt text-primary"></i> {{ __('Dashboard') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('categories.index')}}">
                        <i class="fas fa-folder text-blue"></i> {{ __('Category') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('sub-categories.index')}}">
                        <i class="fas fa-sitemap text-blue"></i> {{ __('Sub Categories') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('fuel.index')}}">
                        <i class="fas fa-gas-pump text-blue"></i> {{ __('Fuel') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('vehicle-brand.index')}}">
                        <i class="fas fa-bullseye text-blue"></i> {{ __('Vehicle Brand') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('vehicle-model.index')}}">
                        <i class="fas fa-car-side text-blue"></i> {{ __('Vehicle Model') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('vehicle-size.index')}}">
                        <i class="fas fa-search-minus text-blue"></i> {{ __('Vehicle Size') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{route('region.index')}}">
                        <i class="fas fa-globe text-blue"></i> {{ __('Region') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('price-master.index')}}">
                        <i class="far fa-money-bill-alt text-blue"></i>
                         {{ __('Price Master') }}
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{route('point-package.index')}}">
                        <i class="fas fa-exchange-alt text-blue"></i> {{ __('Point Package') }}
                    </a>
                </li>
                 <li class="nav-item ">
                    <a class="nav-link" href="{{route('notice.index')}}">
                        <i class="fas fa-exclamation-triangle text-blue"></i> {{ __('Notice') }}
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="{{route('withdrawal.index')}}">
                        <i class="fas fa-project-diagram text-blue"></i> {{ __('Withdrawal Request') }}
                    </a>
                </li>

                <li class="nav-item">

                    <a class="nav-link" href="{{route('appuser.index')}}">
                        <i class="fas fa-shower text-blue"></i> {{ __('Customer') }}
                    </a>
                </li>

                <li class="nav-item">
                    {{-- appuser.index --}}
                    <a class="nav-link" href="{{route('shopowner.index')}}">
                        <i class="fas fa-store-alt text-blue"></i> {{ __('Shop Owner') }}
                    </a>
                </li>
                <li class="nav-item">
                    {{-- appuser.index --}}
                    <a class="nav-link" href="{{route('inquiry')}}">
                        <i class="fas fa-question text-blue"></i> {{ __('Inquiry') }}
                    </a>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="{{ route('notification.index') }}">
                        <i class="fas fa-bell text-blue"></i> {{ __('Notification') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('tip.index') }}">
                        <i class="fas fa-heart text-blue"></i> {{ __('Tips') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('announce.index') }}">
                        <i class="fas fa-heart text-blue"></i> {{ __('announces') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link active" href="#navbar-examples" data-toggle="collapse" role="button"
                        aria-expanded="true" aria-controls="navbar-examples">
                        <i class="fas fa-file-pdf text-blue"></i>
                        <span class="nav-link-text">{{__('Report')}}</span>
                    </a>
                    <div class="collapse show" id="navbar-examples">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('report.user') }}">
                                    {{__('User Report')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('report.owner') }}">
                                    {{__('Owner Report')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('report.withdrawal') }}">
                                    {{__('Withdrawal Report')}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>


                @can('custom_notification_access')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('custom.index') }}">
                        <i class="fas fa-concierge-bell text-blue"></i> {{ __('Custom Notification') }}
                    </a>
                </li>
                @endcan

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('setting.index') }}">
                        <i class="fas fa-cog text-blue"></i> {{ __('Setting') }}
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('pp') }}">
                        <i class="far fa-handshake text-blue"></i> {{ __('Privacy and Policy') }}
                    </a>
                </li>

                @can('faq_access')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('faq.index') }}">
                        <i class="fas fa-question text-blue"></i> {{ __('FAQ') }}
                    </a>
                </li>
                @endcan


            </ul>

            <hr class="my-3">
        </div>
    </div>
</nav>