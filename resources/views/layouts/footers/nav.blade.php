<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            &copy; {{ now()->year }} <a href="/company-info" class="font-weight-bold ml-1"
                target="_blank"> 회사정보 </a>
            <a class="font-weight-bold ml-1" target="_blank" href="/privacypolicy">개인정보처리방침</a>
            <a class="font-weight-bold ml-1" target="_blank" href="/usepolicy">이용약관</a>
        </div>
        
    </div>

</div>
