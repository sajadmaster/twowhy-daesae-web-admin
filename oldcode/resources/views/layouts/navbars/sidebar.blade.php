<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
            aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('argon') }}/img/brand/logo_blue.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-1-800x800.jpg">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('반갑습니다!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('내 프로필') }}</span>
                    </a>

                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('로그아웃') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                            data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                            aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            {{-- <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended"
                        placeholder="{{ __('Search') }}" aria-label="Search">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-search"></span>
                </div>
            </div>
        </div>
        </form> --}}
        <!-- Navigation -->
        <ul class="navbar-nav">
            <h6 class="navbar-heading text-muted" style="padding: 0rem 1.5rem;">관리자메뉴</h6>
            @can('dashboard')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="fas fa-tachometer-alt text-primary"></i> {{ __('대시보드') }}
                </a>
            </li>
            @endcan
           
            @can('role_access')
            <li class="nav-item">
                <a class="nav-link" href="{{route('roles.index')}}">
                    <i class="fas fa-id-badge text-blue"></i> {{ __('관리자 룰 설정') }}
                </a>
            </li>
            @endcan
            @can('user_access')

            <li class="nav-item">
                <a class="nav-link" href="{{route('users.index')}}">
                    <i class="fas fa-user text-blue"></i> {{ __('플랫폼 전체 사용자') }}
                </a>
            </li>
            @endcan
            @can('category_access')
            <li class="nav-item">
                <a class="nav-link" href="{{route('categories.index')}}">
                    <i class="fas fa-folder text-blue"></i> {{ __('카테고리') }}
                </a>
            </li>
            @endcan
            @can('vehicleBrand_access')
            <li class="nav-item">
                <a class="nav-link" href="{{route('vehicleBrand.index')}}">
                    <i class="fas fa-bullseye text-blue"></i> {{ __('자동차 브랜드') }}
                </a>
            </li>
            @endcan
            @can('vehicleModel_access')
            <li class="nav-item">
                <a class="nav-link" href="{{route('vehicleModel.index')}}">
                    <i class="fas fa-car-side text-blue"></i> {{ __('자동차 모델') }}
                </a>
            </li>
            @endcan
         
          

           
          
            @can('appuser_access')
            <li class="nav-item">
                {{-- appuser.index --}}
                <a class="nav-link" href="{{route('appuser.index')}}">
                    <i class="fas fa-shower text-blue"></i> {{ __('플랫폼 사용자') }}
                </a>
            </li>
            @endcan
            <li class="nav-item">
                {{-- appuser.index --}}
                <a class="nav-link" href="{{route('shopowner.index')}}">
                    <i class="fas fa-store-alt text-blue"></i> {{ __('플랫폼 관리자 및 제휴사') }}
                </a>
            </li>
            {{-- @canany(['booking_access','branch_booking_access'])
            <li class="nav-item">
                <a class="nav-link" href="{{ route('booking.index') }}">
                    <i class="fas fa-cut text-blue"></i> {{ __('전체 예약리스트') }}
                </a>
            </li>
            @endcan --}}
            @can('notification_access')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('notification.index') }}">
                    <i class="fas fa-bell text-blue"></i> {{ __('푸시알림') }}
                </a>
            </li>
            @endcan
            @can('earning_access')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('earning.index') }}">
                    <i class="fas fa-dollar-sign text-blue"></i> {{ __('정산요청') }}
                </a>
            </li>
            @endcan
            {{-- @can('report_access')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('report.index') }}">
                    <i class="far fa-file-word text-blue"></i> {{ __('플랫폼 보고서') }}
                </a>
            </li>
            @endcan --}}
            @can('custom_notification_access')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('custom.index') }}">
                    <i class="fas fa-concierge-bell text-blue"></i> {{ __('푸시메세지 보내기') }}
                </a>
            </li>
            @endcan
            @can('setting_access')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('setting.index') }}">
                    <i class="fas fa-cog text-blue"></i> {{ __('설정') }}
                </a>
            </li>
            @endcan
            @can('privacy_access')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('pp') }}">
                    <i class="far fa-handshake text-blue"></i> {{ __('이용약관') }}
                </a>
            </li>
            @endcan
            @can('faq_access')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('faq.index') }}">
                    <i class="fas fa-question text-blue"></i> {{ __('자주묻는질문') }}
                </a>
            </li>
            @endcan
            {{-- @canany(['review_access','branch_review_access'])
            <li class="nav-item">
                <a class="nav-link" href="{{ route('review.index') }}">
                    <i class="far fa-smile-beam text-blue"></i> {{ __('리뷰') }}
                </a>
            </li>
            @endcan --}}
        

        </ul>
   
        <hr class="my-3">
    </div>
    </div>
</nav>