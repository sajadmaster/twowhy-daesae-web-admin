<?php

return array(
	'app_id' => env('APP_ID'),
	'rest_api_key' => env('REST_API_KEY'),
	'user_auth_key' => env('USER_AUTH_KEY'),
);