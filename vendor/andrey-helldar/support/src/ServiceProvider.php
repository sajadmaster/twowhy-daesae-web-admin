<?php

namespace Helldar\Support;

/**
 * @deprecated Will be removed from version 2.0
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
}
