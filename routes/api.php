<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\FuelController;
use App\Http\Controllers\Admin\PointPackageController;
use App\Http\Controllers\Admin\PriceMasterController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\TipController;
use App\Http\Controllers\Admin\announceController;
use App\Http\Controllers\Admin\VehicleBrandController;
use App\Http\Controllers\Admin\VehicleModelController;
use App\Http\Controllers\Admin\VehicleSizeController;
use App\Http\Controllers\AppUsersController;
use App\Http\Controllers\BookingMasterController;
use App\Http\Controllers\InquiryController;
use App\Http\Controllers\Owner\EmployeeController;
use App\Http\Controllers\Owner\OwnerManagerController;
use App\Http\Controllers\Owner\OwnerPackageController;
use App\Http\Controllers\Owner\OwnerProductController;
use App\Http\Controllers\Owner\OwnerServiceController;
use App\Http\Controllers\Owner\OwnerShopController;
use App\Http\Controllers\Owner\ShopOwnerController;
use App\Http\Controllers\ProductOrderController;
use App\Http\Controllers\ProductReviewController;
use App\Http\Controllers\UserAddressController;
use App\Http\Controllers\UserVehicleController;
use App\Http\Controllers\WithdrawalApplicationController;
use App\Models\OwnerManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\NoticeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'user'], function () {
       Route::get('order/product/{id}', [AppUsersController::class, 'getownerTel']);
    Route::get('price-table', [PriceMasterController::class, 'apiIndex']);
    Route::post('price-table/single', [PriceMasterController::class, 'singlePrice']);
    Route::get('vehicle-size', [VehicleSizeController::class, 'apiIndex']);
    Route::get('tetstagain', [AppUsersController::class, 'tetstagain']);
    Route::post('login', [AppUsersController::class, 'login']);
    Route::post('social/login', [AppUsersController::class, 'socialLogin']);
    Route::post('forgot', [AppUsersController::class, 'forgotpassword']);
    Route::post('register', [AppUsersController::class, 'store']);
    Route::get('booking-status/{id}', [AppUsersController::class, 'isBooking']);

    //
    Route::get('category', [CategoryController::class, 'apiIndex']);
    Route::get('category/{id}/sub', [CategoryController::class, 'subCategory']);
    Route::get('region', [RegionController::class, 'apiIndexAll']);
    Route::get('region/{id}/shop', [OwnerShopController::class, 'shopByRegion']);
    // 카테고리별 정렬
    Route::post('region/{id}/shopcate', [OwnerShopController::class, 'shopByRegionC']);
    Route::get('shop/{id}/service', [OwnerShopController::class, 'shopService']);

    Route::get('privacy', [AppUsersController::class, 'privacy']);
    Route::get('aboutus', [AppUsersController::class, 'aboutus']);
    Route::get('setting', [AppUsersController::class, 'setting']);
    Route::get('tips', [TipController::class, 'apiIndex']);
    Route::get('tip/{id}', [TipController::class, 'singleTip']);
    // 배너 알림 게시판
    Route::get('announce', [announceController::class, 'apiIndex']);
    Route::get('announce/{id}', [announceController::class, 'singleannounce']);
    
    Route::get('notices', [NoticeController::class, 'apiIndex']);
    Route::get('notice/{id}', [NoticeController::class, 'singleNotice']);
    Route::get('get-vehicle/{id}', [AppUsersController::class, 'getVehicleImage']);

    Route::post('sub/shop', [OwnerShopController::class, 'shopForProduct']);
    Route::get('shop/{id}/product', [OwnerShopController::class, 'shopProduct']);
    Route::get('product/{id}', [OwnerShopController::class, 'productView']);

    Route::get('vehicle-brand', [VehicleBrandController::class, 'apiIndex']);
    Route::get('vehicle-brand/{id}/model', [VehicleBrandController::class, 'vehicleModel']);
    Route::get('model/{id}', [VehicleModelController::class, 'vehicleModelName']);
    Route::get('fuel', [FuelController::class, 'apiIndex']);
    Route::group(['middleware' => ['auth:appUser']], function () {
        Route::post('child/{id}/update', [BookingMasterController::class, 'childUpdate']);
        //전후사진보기
        Route::get('child/{id}/beforeimg', [AppUsersController::class, 'beforeImg']);
        Route::get('child/{id}/afterimg', [AppUsersController::class, 'afterImg']);
        Route::post('booking/{id}/update', [BookingMasterController::class, 'updateBooking']);
        Route::post('vehicle', [UserVehicleController::class, 'store']);
        Route::get('vehicle', [UserVehicleController::class, 'index']);
        Route::get('point-package', [PointPackageController::class, 'apiIndex']);
        Route::post('point-package', [PointPackageController::class, 'buyNewPoint']);
        Route::get('point', [PointPackageController::class, 'point']);
        // 첫가입시 포인트 적립
        Route::get('newuserpoint', [AppUsersController::class, 'newuserpoint']);
        
        Route::get('vehicle/{id}', [UserVehicleController::class, 'show']);
        Route::get('vehicle/{id}/point', [UserVehicleController::class, 'pointVehicle']);
        Route::post('vehicle/{id}/detail', [UserVehicleController::class, 'detailVehicle']);
        Route::post('vehicle/{id}', [UserVehicleController::class, 'update']);
        Route::get('vehicle/{id}/delete', [UserVehicleController::class, 'destroy']);
         Route::post('vehicle/{id}/update', [UserVehicleController::class, 'update']);
        Route::post('address', [UserAddressController::class, 'store']);
        Route::get('address', [UserAddressController::class, 'index']);
        Route::get('address/{id}', [UserAddressController::class, 'show']);
        Route::post('address/{id}', [UserAddressController::class, 'update']);
        // 결제 포인트 적립
        Route::post('pointAdd', [AppUsersController::class, 'pointAdd']);
        
        Route::post('order/product', [ProductOrderController::class, 'store']);
        Route::post('booking', [BookingMasterController::class, 'store']);
        Route::get('booking', [BookingMasterController::class, 'index']);
        Route::get('future/booking', [BookingMasterController::class, 'futureBooking']);
        Route::post('future/booking/date', [BookingMasterController::class, 'futureBookingDate']);
        Route::get('schedule/booking', [BookingMasterController::class, 'scheduleBooking']);
        Route::post('booking/{id}', [BookingMasterController::class, 'updateBooking']);
        Route::get('order/product', [ProductOrderController::class, 'orderList']);
        Route::post('inquiry', [InquiryController::class, 'store']);
        Route::post('order/product/review', [ProductReviewController::class, 'store']);
        Route::post('booking/service/review', [ProductReviewController::class, 'bookingReview']);
        
        Route::post('profile/password/update', [AppUsersController::class, 'password']);
        Route::post('profile/picture/update', [AppUsersController::class, 'profilePictureUpdate']);
        Route::post('newpassword', [AppUsersController::class, 'newPassword']);
        Route::post('profile/update', [AppUsersController::class, 'profileUpdate']);
        Route::get('profile', [AppUsersController::class, 'profile']);
        Route::get('review', [AppUsersController::class, 'reviewList']);
        // 위치 이미지 로드
        Route::get('locationimage', [AppUsersController::class, 'LocationImageLoad']);
        // 전체 리뷰
        Route::get('reviewListAll', [AppUsersController::class, 'reviewListAll']);
        Route::post('review/product/{id}/update', [ProductReviewController::class, 'productReviewUpdate']);
        // Route::post('review/product/{id}/update/image', [AppUsersController::class, 'productReviewImageAdd']);
        Route::get('review/product/{id}/delete/image', [ProductReviewController::class, 'productReviewImageDelete']);
            // 유저 차량 위치 등록
        Route::post('add-vehicle-location', [AppUsersController::class, 'addVehicleLocation']);
        Route::post('add/vehiclelocation/info', [AppUsersController::class, 'addVehicleLocationInfo']);
        Route::post('add-vehicle-location-detail', [AppUsersController::class, 'addVehicleLocationDetail']);
        
        
        
        Route::post('review/booking/{id}/update', [ProductReviewController::class, 'bookingReviewUpdate']);
        // Route::post('review/product/{id}/update/image', [AppUsersController::class, 'productReviewImageAdd']);
        Route::get('review/booking/{id}/delete/image', [ProductReviewController::class, 'bookingReviewImageDelete']);
        
        
        Route::get('notification', [AppUsersController::class, 'notificationUser']);
    });
});

Route::group(['prefix' => 'owner'], function () {
    Route::post('register', [ShopOwnerController::class, 'store']);
    Route::post('login', [ShopOwnerController::class, 'login']);
    Route::get('tips', [TipController::class, 'apiIndex']);
    Route::post('social/login', [AppUsersController::class, 'socialLoginOwner']);

    Route::get('setting', [AppUsersController::class, 'setting']);

    Route::get('tip/{id}', [TipController::class, 'singleTip']);
    Route::group(['middleware' => ['auth:shopOwner']], function () {
        Route::get('notification', [AppUsersController::class, 'notificationOwner']);

        Route::post('profile/password/update', [AppUsersController::class, 'password']);
        Route::post('profile/picture/update', [AppUsersController::class, 'profilePictureUpdate']);
        Route::post('profile/update', [AppUsersController::class, 'profileUpdate']);
        Route::post('newpassword', [AppUsersController::class, 'newPassword']);
        Route::get('profile', [AppUsersController::class, 'profile']);

        Route::get('review', [AppUsersController::class, 'reviewListOwner']);
        Route::get('point', [PointPackageController::class, 'pointOwner']);
        Route::post('withdrawal', [WithdrawalApplicationController::class, 'reqNew']);
        Route::get('withdrawal', [WithdrawalApplicationController::class, 'OwnerIndex']);
        Route::get('category', [CategoryController::class, 'apiIndex']);
        Route::get('region', [RegionController::class, 'apiIndex']);
        Route::get('vehicle-size', [VehicleSizeController::class, 'apiIndex']);
        Route::get('category/{id}/sub', [CategoryController::class, 'subCategory']);
        Route::post('category/sub', [CategoryController::class, 'multiSubCategory']);
        Route::post('manager', [OwnerManagerController::class, 'store']);
        Route::get('manager', [OwnerManagerController::class, 'index']);
        Route::post('manager/{id}', [OwnerManagerController::class, 'update']);
        Route::get('manager/{id}', [OwnerManagerController::class, 'show']);
        Route::delete('manager/{id}', [OwnerManagerController::class, 'destroy']);
        Route::get('shop-employee/{id}', [OwnerShopController::class, 'getShopEmployee']);


        Route::get('booking', [BookingMasterController::class, 'ownerIndex']);
        Route::post('booking/{id}/update', [BookingMasterController::class, 'updateBooking']);
        Route::post('order/{id}/update', [ProductOrderController::class, 'updateOrder']);

        Route::get('order/product', [ProductOrderController::class, 'orderOwnerList']);
        Route::resources([
            'service' => OwnerServiceController::class,
            'employee' => EmployeeController::class,
            'product' => OwnerProductController::class,
            'package' => OwnerPackageController::class,
            'shop' => OwnerShopController::class,
        ]);
        Route::delete('image/{id}/service', [OwnerServiceController::class, 'deleteImage']);
        Route::post('image/{id}/service', [OwnerServiceController::class, 'addImage']);
        Route::delete('image/{id}/product', [OwnerProductController::class, 'deleteImage']);
        Route::post('image/{id}/product', [OwnerProductController::class, 'addImage']);
        Route::delete('image/{id}/package', [OwnerPackageController::class, 'deleteImage']);
        Route::post('image/{id}/package', [OwnerPackageController::class, 'addImage']);
        Route::delete('price/{id}/service', [OwnerServiceController::class, 'deletePrice']);
        Route::post('price/service', [OwnerServiceController::class, 'newPrice']);
        
        
        Route::get('new/booking/wait', [BookingMasterController::class, 'waitingBookingManger']);
        // 완료된 목록보기
        Route::post('filter/booking/complete', [EmployeeController::class, 'bookingBetweenComplete_owner']);
        Route::post('filter/order/complete', [EmployeeController::class, 'orderBetweenComplete_owner']);
        
        Route::get('accumulate/{id}', [OwnerShopController::class, 'getEmployeePoint']);
        Route::get('child/{id}/beforeimg', [AppUsersController::class, 'beforeImg']);
        Route::get('child/{id}/afterimg', [AppUsersController::class, 'afterImg']);
        // 전체 리뷰
        Route::get('reviewListAll', [AppUsersController::class, 'reviewListAll']);
        
    });
});

Route::group(['prefix' => 'manager'], function () {
    Route::get('notices', [NoticeController::class, 'apiIndexManager']);
    Route::get('notice/{id}', [NoticeController::class, 'singleNotice']);
    Route::post('login', [OwnerManagerController::class, 'login']);
    Route::get('tips', [TipController::class, 'apiIndex']);
    Route::get('tip/{id}', [TipController::class, 'singleTip']);
    Route::group(['middleware' => ['auth:manager']], function () {
        Route::post('profile/password/update', [AppUsersController::class, 'password']);
        Route::post('profile/picture/update', [AppUsersController::class, 'profilePictureUpdate']);
        Route::post('profile/update', [AppUsersController::class, 'profileUpdate']);
        Route::post('newpassword', [AppUsersController::class, 'newPassword']);
        Route::get('profile', [AppUsersController::class, 'profile']);
        Route::get('vehicle-size', [VehicleSizeController::class, 'apiIndex']);
            Route::get('accumulate/{id}', [EmployeeController::class, 'getAccumulatePoint']);

        Route::resources([
            'service' => OwnerServiceController::class,
            'employee' => EmployeeController::class,
            'product' => OwnerProductController::class,
            'package' => OwnerPackageController::class,
            'shop' => OwnerShopController::class,
        ]);

        Route::get('category', [CategoryController::class, 'apiIndex']);
        Route::get('region', [RegionController::class, 'apiIndex']);
        Route::get('category/{id}/sub', [CategoryController::class, 'subCategory']);
        Route::post('category/sub', [CategoryController::class, 'multiSubCategory']);
        Route::get('manager', [OwnerManagerController::class, 'index']);
        Route::get('shop-employee/{id}', [OwnerShopController::class, 'getShopEmployee']);

        Route::get('booking', [BookingMasterController::class, 'ownerIndex']);

        Route::post('booking/{id}/update', [BookingMasterController::class, 'updateBooking']);
        Route::post('order/{id}/update', [ProductOrderController::class, 'updateOrder']);
        Route::get('order/product', [ProductOrderController::class, 'orderOwnerList']);
        Route::get('review', [AppUsersController::class, 'reviewListOwner']);


        Route::get('new/booking/wait', [BookingMasterController::class, 'waitingBookingManger']);
        // 완료된 목록보기
        Route::post('filter/booking/complete', [EmployeeController::class, 'bookingBetweenComplete_owner']);
        Route::post('filter/order/complete', [EmployeeController::class, 'orderBetweenComplete_owner']);
        Route::get('child/{id}/beforeimg', [AppUsersController::class, 'beforeImg']);
        Route::get('child/{id}/afterimg', [AppUsersController::class, 'afterImg']);
        // 전체 리뷰
        Route::get('reviewListAll', [AppUsersController::class, 'reviewListAll']);
    });
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('setting', [AppUsersController::class, 'setting']);

    Route::get('tips', [TipController::class, 'apiIndex']);
    Route::get('tip/{id}', [TipController::class, 'singleTip']);
    Route::post('login', [EmployeeController::class, 'login']);
    Route::group(['middleware' => ['auth:employee']], function () {
        Route::post('profile/password/update', [AppUsersController::class, 'password']);
        Route::get('notification', [AppUsersController::class, 'notificationEmployee']);

        Route::post('profile/picture/update', [AppUsersController::class, 'profilePictureUpdate']);
        Route::post('newpassword', [AppUsersController::class, 'newPassword']);
        Route::post('profile/update', [AppUsersController::class, 'profileUpdate']);
        Route::get('profile', [AppUsersController::class, 'profile']);
        Route::get('booking', [EmployeeController::class, 'bookingApi']);
        //누적 포인트 테스트
        Route::get('accumulate/{id}', [EmployeeController::class, 'getAccumulatePoint']);
        Route::get('booking/{id}', [EmployeeController::class, 'singleBooking']);
        Route::post('filter/booking', [EmployeeController::class, 'bookingBetween']);
        // 완료된 목록보기
        Route::post('filter/booking/complete', [EmployeeController::class, 'bookingBetweenComplete']);
        Route::post('filter/order/complete', [EmployeeController::class, 'orderBetweenComplete']);
        Route::post('booking/{id}/update', [BookingMasterController::class, 'updateBooking']);
        Route::post('child/{id}/update', [BookingMasterController::class, 'childUpdate']);
        Route::get('order', [EmployeeController::class, 'bookingApiProduct']);
        Route::post('order/{id}/update', [ProductOrderController::class, 'updateOrder']);
        Route::post('filter/order', [EmployeeController::class, 'orderBetween']);
        Route::get('new/booking/wait', [BookingMasterController::class, 'waitingBookingEmployee']);
        //
        Route::post('order/product/sc_store', [ProductReviewController::class, 'store']);
        Route::post('booking/service/start', [ProductReviewController::class, 'workStart']);
        Route::post('booking/service/end', [ProductReviewController::class, 'workEnd']);
        Route::post('child-count', [ProductReviewController::class, 'childCount']);
        Route::post('working-start', [ProductReviewController::class, 'workingStart']);
        Route::post('working-end', [ProductReviewController::class, 'workingEnd']);
        Route::post('working-end-4', [ProductReviewController::class, 'workingEnd4']);
        Route::post('working-not-end-4', [ProductReviewController::class, 'workingNotEnd']);
        Route::post('add/startworking/info', [AppUsersController::class,
            'addStartworkingInfo']);
        Route::post('add/endworking/info', [AppUsersController::class,
            'addEndworkingInfo']);
        // 위치 이미지 로드
        Route::post('get-location-image', [ProductReviewController::class, 'getLocationImageLoad']);
        //
        // 전체 리뷰
        Route::get('reviewListAll', [AppUsersController::class, 'reviewListAll']);
        Route::get('child/{id}/beforeimg', [AppUsersController::class, 'beforeImg']);
        Route::get('child/{id}/afterimg', [AppUsersController::class, 'afterImg']);
    });
});
