@extends('layouts.app')

@section('content')
@include('layouts.headers.header',
array(
'class'=>'info',
'title'=>__("Withdrawal Application"),'description'=>'',
'icon'=>'fas fa-home',
'breadcrumb'=>array([
'text'=>__('Fuel'),
'text'=>__('Edit Withdrawal Application')
])))
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-6">
            <div class="card shadow">
                <div class="card-header ">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Owner Detail') }}</h3>
                        </div>

                    </div>
                </div>

                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-auto">
                            <!-- Avatar -->
                            <a href="#" class="avatar avatar-xl rounded-circle">
                                <img alt="Image placeholder"
                                    src="{{ asset('upload') .'/'.$data->owner->image}}">
                            </a>
                        </div>
                        <div class="col ml--2">
                            <h4 class="mb-0">
                                <a href="#!">{{$data->owner->name}}</a>
                            </h4>
                            <p class="text-sm text-muted mb-0">{{$data->owner->email}}</p>
                         <span class="text-{{$data->owner->status ? 'success' : 'danger'}}">●</span>
                        <small>{{$data->owner->status ? 'Active' : 'Disable'}}</small>
                        </div>
                        <div class="col-auto">
                            <a type="button" href="{{ route('shopowner.show', ['id'=> $data->owner->id]) }}" class="btn btn-sm btn-primary">{{__('View')}}</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card-profile-stats d-flex justify-content-center">
                                <div>
                                    <span class="heading text-success">{{$data->owner->balance}}</span>
                                    <span class="description">{{__('Available Point')}}</span>
                                </div>
                                <div>
                                    <span class="heading text-danger">{{$data->req_point}}</span>
                                    <span class="description">{{__('Requested Point')}}</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="col-6">
            <div class="card shadow">
                <div class="card-header ">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Edit Withdrawal Application') }}</h3>
                        </div>

                    </div>
                </div>

                <div class="card-body">

                    <form enctype="multipart/form-data" action="{{ route("withdrawal.update", [$data->id]) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('Remark:')}}</label>
                                    <input type="text" name="remark" value="{{ old('remark',$data->remark) }}"
                                        class="form-control  @error('remark') invalid-input @enderror"
                                        placeholder="{{__('Please Enter remark')}}" autofocus>

                                    @error('remark')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('Transaction ID:')}}</label>
                                    <input type="text" name="transaction_id"
                                        value="{{ old('transaction_id',$data->transaction_id) }}"
                                        class="form-control  @error('transaction_id') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Transaction ID')}}">

                                    @error('transaction_id')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('Status')}}</label>
                                    <select name="status" class="form-control  @error('transaction_id') invalid-input @enderror">
                                        <option value="0" {{$data->status == 0 ? "selected" : ""}}>{{__('Requested')}}</option>
                                        <option value="1" {{$data->status == 1 ? "selected" : ""}}>{{__('In Progress')}}</option>
                                        <option value="2" {{$data->status == 2 ? "selected" : ""}}>{{__('Complate')}}</option>
                                        <option value="4" {{$data->status == 4 ? "selected" : ""}}>{{__('Cancel')}}</option>
                                    </select>

                                    @error('status')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>






                        </div>
                        @if ($data->status < 2) <button class="btn btn-primary" type="submit">{{__('Submit')}}</button>

                            @endif
                    </form>
                </div>



            </div>
        </div>
    </div>
</div>
@endsection