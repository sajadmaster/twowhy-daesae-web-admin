@extends('layouts.app')

@section('content')
@include('layouts.headers.header',
array(
'class'=>'info',
'title'=>__("Price Master"),'description'=>'',
'icon'=>'fas fa-home',
'breadcrumb'=>array([
'text'=>__('Price Master'),
'text'=>__('New Price Master'),
])))
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header ">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('New Price Master Detail') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('fuel.index') }}"
                                class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form action="{{ route('price-master.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="validationDefault01">{{__('Name:')}}</label>
                                    <input type="text" name="name" value="{{ old('name') }}"
                                        class="form-control  @error('name') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Name')}}" required>

                                    @error('name')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('In Count:')}}</label>
                                    <input type="number" name="in_count" value="{{ old('in_count') }}"
                                        class="form-control  @error('in_count') invalid-input @enderror"
                                        placeholder="{{__('Please Enter In Count')}}">

                                    @error('in_count')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('Out Count:')}}</label>
                                    <input type="number" name="out_count" value="{{ old('out_count') }}"
                                        class="form-control  @error('out_count') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Out Count')}}">

                                    @error('out_count')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-12 mb-3">
                                <div class="form-group d-flex">
                                    <label class="form-control-label" for="validationDefault01">{{__('Type:')}}</label>
                                    <label class="custom-toggle custom-toggle-primary ml-2">
                                        <input id="MaterType" type="checkbox" value="1" name="type">
                                        <span class="custom-toggle-slider rounded-circle" data-label-off="No"
                                            data-label-on="Yes"></span>
                                    </label>
                                    @error('type')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror

                                </div>
                            </div>
                            @foreach ($size as $s)

                            <div class="col-md-4 mb-3" style="display: flex;    align-items: center;">
                                <h1>{{$s->name}}</h1>
                                <input type="hidden" name="size_id[]" value="{{$s->id}}">
                            </div>
                            <div class="col-md-4 mb-3 normal-price">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('In Price:')}}</label>
                                    <input type="number" name="in_price[]" value="{{ old('in_price'.$loop->index) }}"
                                        class="form-control  @error('in_price') invalid-input @enderror"
                                        placeholder="{{__('Please Enter In Price')}}">
                                    @error('in_price')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 mb-3 normal-price">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('Out Price:')}}</label>
                                    <input type="number" name="out_price[]" value="{{ old('out_price'.$loop->index) }}"
                                        class="form-control  @error('out_price') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Out Price')}}">

                                    @error('out_price')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-8 mb-3 mega-price" style="display: none">
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="validationDefault01">{{__('Mega Price:')}}</label>
                                    <input type="number" name="mega_price[]"
                                        value="{{ old('mega_price'.$loop->index) }}"
                                        class="form-control  @error('mega_price') invalid-input @enderror"
                                        placeholder="{{__('Please Enter Mega Price')}}">

                                    @error('mega_price')
                                    <div class="invalid-div">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <button class="btn btn-primary" type="submit">{{__('Submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection