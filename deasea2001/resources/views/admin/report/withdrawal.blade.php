@extends('layouts.app')

@section('content')
@include('layouts.headers.header',
array(
'class'=>'info',
'title'=>__("Withdrawal"),'description'=>'',
'icon'=>'fas fa-home',
'breadcrumb'=>array([
'text'=>__('Withdrawal List')
])))
<div class="container-fluid mt--7">

    <div class="row">
        <div class="col-12 mb-5">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <form action="{{ route('report.withdrawal' ) }}" method="POST" class="d-flex">
                        @csrf

                        <div class="form-group mr-5">
                            <label>{{ __('Start Date') }}</label>
                            <input type="date" name="start_date"
                                class="form-control @error('start_date') is-invalid @enderror" required
                                value="{{ old('start_date') }}">
                        </div>
                        <div class="form-group mr-5">
                            <label>{{ __('End Date') }}</label>
                            <input type="date" name="end_date"
                                class="form-control @error('end_date') is-invalid @enderror" required
                                value="{{ old('end_date') }}">
                        </div>
                        <button style="height: 40px;" class="btn btn-success mt-4"
                            type="submit">{{ __('Search') }}</button>
                    </form>
                </div>
            </div>
        </div>

        
    </div>
    <div class="row mt-5">
        <div class="col">
            <div class="card shadow">
                <div class="card-header mb-3">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Withdrawal') }}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>
                <div class="table-responsive py-4">
                  <table id="dataTable" class="table table-flush">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>{{__('Owner Name')}}</th>
                            <th>{{__('Request Point')}}</th>
                            <th>{{__('Transaction ID')}}</th>
                            <th>{{__('Remark')}}</th>
                            <th>{{__('Status')}}</th>
                            <th>{{__('Last Updated')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $cat)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{ $cat->owner->name ?? "NO Data"}}
                            <td>{{ $cat->req_point }}
                            <td>{{ $cat->transaction_id ?? "No Data"}}
                            <td>{{ $cat->remark ?? "No Data"}}
                            </td>
                            <td>
                                @if ($cat->status == 0)
                                <span class="badge badge-info m-1">{{__('requested')}}</span>
                                @elseif($cat->status == 1)
                                <span class="badge badge-warning  m-1">{{__('In Progress')}}</span>
                                @elseif($cat->status == 2)
                                <span class="badge badge-success  m-1">{{__('Complate')}}</span>
                                @else
                                <span class="badge badge-danger  m-1">{{__('Cancel')}}</span>
                                @endif
                            </td>
                          
                            <td>{{$cat->updated_at}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection