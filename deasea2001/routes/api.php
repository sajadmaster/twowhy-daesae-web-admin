<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\FuelController;
use App\Http\Controllers\Admin\PointPackageController;
use App\Http\Controllers\Admin\PriceMasterController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\TipController;
use App\Http\Controllers\Admin\VehicleBrandController;
use App\Http\Controllers\Admin\VehicleSizeController;
use App\Http\Controllers\AppUsersController;
use App\Http\Controllers\BookingMasterController;
use App\Http\Controllers\InquiryController;
use App\Http\Controllers\Owner\EmployeeController;
use App\Http\Controllers\Owner\OwnerManagerController;
use App\Http\Controllers\Owner\OwnerPackageController;
use App\Http\Controllers\Owner\OwnerProductController;
use App\Http\Controllers\Owner\OwnerServiceController;
use App\Http\Controllers\Owner\OwnerShopController;
use App\Http\Controllers\Owner\ShopOwnerController;
use App\Http\Controllers\ProductOrderController;
use App\Http\Controllers\ProductReviewController;
use App\Http\Controllers\UserAddressController;
use App\Http\Controllers\UserVehicleController;
use App\Http\Controllers\WithdrawalApplicationController;
use App\Models\OwnerManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'user'], function () {
    Route::get('price-table', [PriceMasterController::class, 'apiIndex']);
    Route::post('price-table/single', [PriceMasterController::class, 'singlePrice']);
    Route::get('vehicle-size', [VehicleSizeController::class, 'apiIndex']);
    Route::get('tetstagain', [AppUsersController::class, 'tetstagain']);
    Route::post('login', [AppUsersController::class, 'login']);
    Route::post('social/login', [AppUsersController::class, 'socialLogin']);
    Route::post('register', [AppUsersController::class, 'store']);
    Route::get('category', [CategoryController::class, 'apiIndex']);
    Route::get('category/{id}/sub', [CategoryController::class, 'subCategory']);
    Route::get('region', [RegionController::class, 'apiIndexAll']);
    Route::get('region/{id}/shop', [OwnerShopController::class, 'shopByRegion']);
    Route::get('shop/{id}/service', [OwnerShopController::class, 'shopService']);

    Route::get('privacy', [AppUsersController::class, 'privacy']);
    Route::get('aboutus', [AppUsersController::class, 'aboutus']);
    Route::get('setting', [AppUsersController::class, 'setting']);
    Route::get('tips', [TipController::class, 'apiIndex']);
    Route::get('tip/{id}', [TipController::class, 'singleTip']);

    Route::post('sub/shop', [OwnerShopController::class, 'shopForProduct']);
    Route::get('shop/{id}/product', [OwnerShopController::class, 'shopProduct']);
    Route::get('product/{id}', [OwnerShopController::class, 'productView']);

    Route::get('vehicle-brand', [VehicleBrandController::class, 'apiIndex']);
    Route::get('vehicle-brand/{id}/model', [VehicleBrandController::class, 'vehicleModel']);
    Route::get('fuel', [FuelController::class, 'apiIndex']);
    Route::group(['middleware' => ['auth:appUser']], function () {
        Route::post('child/{id}/update', [BookingMasterController::class, 'childUpdate']);
        Route::post('booking/{id}/update', [BookingMasterController::class, 'updateBooking']);
        Route::post('vehicle', [UserVehicleController::class, 'store']);
        Route::get('vehicle', [UserVehicleController::class, 'index']);
        Route::get('point-package', [PointPackageController::class, 'apiIndex']);
        Route::post('point-package', [PointPackageController::class, 'buyNewPoint']);
        Route::get('point', [PointPackageController::class, 'point']);
        Route::get('vehicle/{id}', [UserVehicleController::class, 'show']);
        Route::post('address', [UserAddressController::class, 'store']);
        Route::get('address', [UserAddressController::class, 'index']);
        Route::get('address/{id}', [UserAddressController::class, 'show']);
        Route::post('order/product', [ProductOrderController::class, 'store']);
        Route::post('booking', [BookingMasterController::class, 'store']);
        Route::get('booking', [BookingMasterController::class, 'index']);
        Route::get('future/booking', [BookingMasterController::class, 'futureBooking']);
        Route::post('future/booking/date', [BookingMasterController::class, 'futureBookingDate']);
        Route::get('schedule/booking', [BookingMasterController::class, 'scheduleBooking']);
        Route::post('booking/{id}', [BookingMasterController::class, 'updateBooking']);
        Route::get('order/product', [ProductOrderController::class, 'orderList']);
        Route::post('inquiry', [InquiryController::class, 'store']);
        Route::post('order/product/review', [ProductReviewController::class, 'store']);
        Route::post('booking/service/review', [ProductReviewController::class, 'bookingReview']);
        Route::post('profile/password/update', [AppUsersController::class, 'password']);
        Route::post('profile/picture/update', [AppUsersController::class, 'profilePictureUpdate']);
        Route::post('newpassword', [AppUsersController::class, 'newPassword']);
        Route::post('profile/update', [AppUsersController::class, 'profileUpdate']);
        Route::get('profile', [AppUsersController::class, 'profile']);
        Route::get('review', [AppUsersController::class, 'reviewList']);
        Route::get('notification', [AppUsersController::class, 'notificationUser']);
    });
});

Route::group(['prefix' => 'owner'], function () {
    Route::post('register', [ShopOwnerController::class, 'store']);
    Route::post('login', [ShopOwnerController::class, 'login']);
    Route::get('tips', [TipController::class, 'apiIndex']);
    Route::post('social/login', [AppUsersController::class, 'socialLoginOwner']);

    Route::get('setting', [AppUsersController::class, 'setting']);

    Route::get('tip/{id}', [TipController::class, 'singleTip']);
    Route::group(['middleware' => ['auth:shopOwner']], function () {
        Route::get('notification', [AppUsersController::class, 'notificationOwner']);

        Route::post('profile/password/update', [AppUsersController::class, 'password']);
        Route::post('profile/picture/update', [AppUsersController::class, 'profilePictureUpdate']);
        Route::post('profile/update', [AppUsersController::class, 'profileUpdate']);
        Route::post('newpassword', [AppUsersController::class, 'newPassword']);
        Route::get('profile', [AppUsersController::class, 'profile']);

        Route::get('review', [AppUsersController::class, 'reviewListOwner']);
        Route::get('point', [PointPackageController::class, 'pointOwner']);
        Route::post('withdrawal', [WithdrawalApplicationController::class, 'reqNew']);
        Route::get('withdrawal', [WithdrawalApplicationController::class, 'OwnerIndex']);
        Route::get('category', [CategoryController::class, 'apiIndex']);
        Route::get('region', [RegionController::class, 'apiIndex']);
        Route::get('vehicle-size', [VehicleSizeController::class, 'apiIndex']);
        Route::get('category/{id}/sub', [CategoryController::class, 'subCategory']);
        Route::post('category/sub', [CategoryController::class, 'multiSubCategory']);
        Route::post('manager', [OwnerManagerController::class, 'store']);
        Route::get('manager', [OwnerManagerController::class, 'index']);
        Route::post('manager/{id}', [OwnerManagerController::class, 'update']);
        Route::get('manager/{id}', [OwnerManagerController::class, 'show']);
        Route::delete('manager/{id}', [OwnerManagerController::class, 'destroy']);
        Route::get('shop-employee/{id}', [OwnerShopController::class, 'getShopEmployee']);


        Route::get('booking', [BookingMasterController::class, 'ownerIndex']);
        Route::post('booking/{id}/update', [BookingMasterController::class, 'updateBooking']);
        Route::post('order/{id}/update', [ProductOrderController::class, 'updateOrder']);

        Route::get('order/product', [ProductOrderController::class, 'orderOwnerList']);
        Route::resources([
            'service' => OwnerServiceController::class,
            'employee' => EmployeeController::class,
            'product' => OwnerProductController::class,
            'package' => OwnerPackageController::class,
            'shop' => OwnerShopController::class,
        ]);
        Route::delete('image/{id}/service', [OwnerServiceController::class, 'deleteImage']);
        Route::post('image/{id}/service', [OwnerServiceController::class, 'addImage']);
        Route::delete('image/{id}/product', [OwnerProductController::class, 'deleteImage']);
        Route::post('image/{id}/product', [OwnerProductController::class, 'addImage']);
        Route::delete('image/{id}/package', [OwnerPackageController::class, 'deleteImage']);
        Route::post('image/{id}/package', [OwnerPackageController::class, 'addImage']);
        Route::delete('price/{id}/service', [OwnerServiceController::class, 'deletePrice']);
        Route::post('price/service', [OwnerServiceController::class, 'newPrice']);
    });
});

Route::group(['prefix' => 'manager'], function () {
    Route::post('login', [OwnerManagerController::class, 'login']);
    Route::get('tips', [TipController::class, 'apiIndex']);
    Route::get('tip/{id}', [TipController::class, 'singleTip']);
    Route::group(['middleware' => ['auth:manager']], function () {
        Route::post('profile/password/update', [AppUsersController::class, 'password']);
        Route::post('profile/picture/update', [AppUsersController::class, 'profilePictureUpdate']);
        Route::post('profile/update', [AppUsersController::class, 'profileUpdate']);
        Route::post('newpassword', [AppUsersController::class, 'newPassword']);
        Route::get('profile', [AppUsersController::class, 'profile']);
        Route::get('vehicle-size', [VehicleSizeController::class, 'apiIndex']);

        Route::resources([
            'service' => OwnerServiceController::class,
            'employee' => EmployeeController::class,
            'product' => OwnerProductController::class,
            'package' => OwnerPackageController::class,
            'shop' => OwnerShopController::class,
        ]);

        Route::get('category', [CategoryController::class, 'apiIndex']);
        Route::get('region', [RegionController::class, 'apiIndex']);
        Route::get('category/{id}/sub', [CategoryController::class, 'subCategory']);
        Route::post('category/sub', [CategoryController::class, 'multiSubCategory']);
        Route::get('manager', [OwnerManagerController::class, 'index']);
        Route::get('shop-employee/{id}', [OwnerShopController::class, 'getShopEmployee']);

        Route::get('booking', [BookingMasterController::class, 'ownerIndex']);

        Route::post('booking/{id}/update', [BookingMasterController::class, 'updateBooking']);
        Route::post('order/{id}/update', [ProductOrderController::class, 'updateOrder']);
        Route::get('order/product', [ProductOrderController::class, 'orderOwnerList']);
        Route::get('review', [AppUsersController::class, 'reviewListOwner']);


        Route::get('new/booking/wait', [BookingMasterController::class, 'waitingBookingManger']);
    });
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('setting', [AppUsersController::class, 'setting']);

    Route::get('tips', [TipController::class, 'apiIndex']);
    Route::get('tip/{id}', [TipController::class, 'singleTip']);
    Route::post('login', [EmployeeController::class, 'login']);
    Route::group(['middleware' => ['auth:employee']], function () {
        Route::post('profile/password/update', [AppUsersController::class, 'password']);
        Route::get('notification', [AppUsersController::class, 'notificationEmployee']);

        Route::post('profile/picture/update', [AppUsersController::class, 'profilePictureUpdate']);
        Route::post('newpassword', [AppUsersController::class, 'newPassword']);
        Route::post('profile/update', [AppUsersController::class, 'profileUpdate']);
        Route::get('profile', [AppUsersController::class, 'profile']);
        Route::get('booking', [EmployeeController::class, 'bookingApi']);
        Route::get('booking/{id}', [EmployeeController::class, 'singleBooking']);
        Route::post('filter/booking', [EmployeeController::class, 'bookingBetween']);
        Route::post('booking/{id}/update', [BookingMasterController::class, 'updateBooking']);
        Route::post('child/{id}/update', [BookingMasterController::class, 'childUpdate']);
        Route::get('order', [EmployeeController::class, 'bookingApiProduct']);
        Route::post('order/{id}/update', [ProductOrderController::class, 'updateOrder']);
        Route::post('filter/order', [EmployeeController::class, 'orderBetween']);
        Route::get('new/booking/wait', [BookingMasterController::class, 'waitingBookingEmployee']);
    });
});
