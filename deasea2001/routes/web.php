<?php

use App\Http\Controllers\Admin\AdminSettingController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\FuelController;
use App\Http\Controllers\Admin\PointPackageController;
use App\Http\Controllers\Admin\PriceMasterController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\StaticNotiController;
use App\Http\Controllers\Admin\SubCategoryController;
use App\Http\Controllers\Admin\TipController;
use App\Http\Controllers\Admin\VehicleBrandController;
use App\Http\Controllers\Admin\VehicleModelController;
use App\Http\Controllers\Admin\VehicleSizeController;
use App\Http\Controllers\AppUsersController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InquiryController;
use App\Http\Controllers\Owner\ShopOwnerController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\WithdrawalApplicationController;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/user/kakao',[AppUsersController::class,'testingKakao']);
Route::get('/user/owner/kakao',[AppUsersController::class,'testingKakaoOwner']);
Route::get('/user/kakao/{id}',[AppUsersController::class,'backToApp'])->name('kakaoBack');



Route::get('/user/naver',[AppUsersController::class,'testingNaver']);
Route::get('/', function () {
	// return view('admin.categories.index');
	return redirect('home');
});
Route::get('/clear-config', function () {
	\Artisan::call('cache:clear');
	\Artisan::call('config:clear');
	return "boom";
	// return view('admin.categories.index');
	return redirect('home');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('local/{lang}', function ($lang) {
	App::setLocale($lang);
	session()->put('locale', $lang);
	return redirect()->back();
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);

	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);

	Route::resources([
		'categories' => CategoryController::class,
		'price-master' => PriceMasterController::class,
		'vehicle-brand' => VehicleBrandController::class,
		'vehicle-model' => VehicleModelController::class,
		'fuel' => FuelController::class,
		'sub-categories' => SubCategoryController::class,
		'vehicle-size' => VehicleSizeController::class,
		'region' => RegionController::class,
		'tip' => TipController::class,
		'point-package' => PointPackageController::class,
		'notification' => StaticNotiController::class,

	]);
	Route::get('inquiry', [InquiryController::class, 'index'])->name('inquiry');
	Route::get('withdrawal', [WithdrawalApplicationController::class, 'index'])->name('withdrawal.index');
	Route::get('withdrawal/{id}', [WithdrawalApplicationController::class, 'show'])->name('withdrawal.show');
	Route::put('withdrawal/{id}', [WithdrawalApplicationController::class, 'update'])->name('withdrawal.update');

	Route::get('pp', [AdminSettingController::class, 'pp'])->name('pp');
	Route::post('pp/update', [AdminSettingController::class, 'updatePP'])->name('pp.update');

	Route::post('onesignal', [StaticNotiController::class, 'updateOnesignl'])->name('onesignal.update');
	Route::post('base', [AdminSettingController::class, 'updateBase'])->name('base.update');
	Route::get('setting', [AdminSettingController::class, 'index'])->name('setting.index');
	Route::post('setting/basic', [AdminSettingController::class, 'basicUpdate'])->name('setting.basic');


	Route::get('shopowner', [ShopOwnerController::class, 'index'])->name('shopowner.index');
	Route::get('shopowner/{id}', [ShopOwnerController::class, 'show'])->name('shopowner.show');
	Route::get('shopowner/{id}/detail', [ShopOwnerController::class, 'shopDetail'])->name('shopowner.detail');
	Route::post('shopowner/status/{id}', [ShopOwnerController::class, 'changeStatus'])->name('shopowner.statusChange');
	Route::post('shopowner/daesea/{id}', [ShopOwnerController::class, 'daeseaChange'])->name('shopowner.daesea');
	Route::get('appuser', [AppUsersController::class, 'index'])->name('appuser.index');
	Route::post('appuser/status/{id}', [AppUsersController::class, 'changeStatus'])->name('appuser.statusChange');



	Route::get('report/user', [ReportController::class, 'userReport'])->name('report.user');
	Route::post('report/user', [ReportController::class, 'userReport'])->name('report.user');

	Route::get('report/owner', [ReportController::class, 'ownerReport'])->name('report.owner');
	Route::post('report/owner', [ReportController::class, 'ownerReport'])->name('report.owner');

	Route::get('report/withdrawal', [ReportController::class, 'withdrawalReport'])->name('report.withdrawal');
	Route::post('report/withdrawal', [ReportController::class, 'withdrawalReport'])->name('report.withdrawal');
});
