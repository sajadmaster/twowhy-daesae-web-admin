<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Bavix\Wallet\Traits\HasWallet;
use Bavix\Wallet\Interfaces\Wallet;

class AppUsers extends Authenticatable implements Wallet
{
    use HasFactory, HasApiTokens, HasWallet;
    protected $fillable = [
        'name', 'email', 'phone_no', 'otp', 'address', 'status', 'image', 'password', 'device_token', 'noti', 'verified', 'provider', 'provider_token'
    ];
    protected $table = 'app_users';
    protected $hidden = [
        'password', 'created_at', 'updated_at'
    ];
    protected $appends = ['imageUri'];
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['image'])) {

            return url('upload/') . '/' . $this->attributes['image'];
        }
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
