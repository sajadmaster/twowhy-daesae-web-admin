<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceMaster extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'type', 'in_count', 'out_count'
    ];
    protected $table = 'package_price_master';
    public function Child()
    {
        return $this->hasMany('App\Models\PriceChild', 'master_id', 'id');
    }
}
