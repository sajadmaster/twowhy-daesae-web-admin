<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceChild extends Model
{
    use HasFactory;
    protected $fillable = [
        'master_id', 'size_id', 'in_price', 'out_price', 'mega_price'
    ];
    protected $table = 'package_price_child';

    public function Size()
    {
        return $this->belongsTo('App\Models\VehicleSize', 'size_id', 'id');
    }
}
