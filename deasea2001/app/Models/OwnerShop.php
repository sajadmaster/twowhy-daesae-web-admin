<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerShop extends Model
{
    use HasFactory;
    protected $fillable = [
        'owner_id', 'name', 'address', 'name', 'phone_no', 'services', 'employee', 'managers', 'products', 'packages', 'start_time', 'end_time', 'status', 'categories', 'sub_categories', 'image', 'regions',
    ];
    protected $table = 'owner_shop';
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    protected $appends = ['imageUri', 'avgRating'];
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['image'])) {

            return url('upload/') . '/' . $this->attributes['image'];
        }
    }
    public function ProductReview()
    {
        return $this->hasMany('App\Models\ProductReview', 'shop_id', 'id');
    }
    public function BookingReview()
    {
        return $this->hasMany('App\Models\BookingReview', 'shop_id', 'id');
    }
    public function Booking()
    {
        return $this->hasMany('App\Models\BookingMaster', 'shop_id', 'id')->orderBy('created_at', 'desc');
    }
    public function Order()
    {
        return $this->hasMany('App\Models\ProductOrder', 'shop_id', 'id')->orderBy('created_at', 'desc');
    }
    public function getManagersAttribute($value)
    {
        return explode(',', $value);
    }
    public function getEmployeeAttribute($value)
    {
        return explode(',', $value);
    }
    public function getServicesAttribute($value)
    {
        return explode(',', $value);
    }
    public function getPackagesAttribute($value)
    {
        return explode(',', $value);
    }
    public function getRegionsAttribute($value)
    {
        return explode(',', $value);
    }
    public function getCategoriesAttribute($value)
    {
        return explode(',', $value);
    }
    public function getProductsAttribute($value)
    {
        return explode(',', $value);
    }
    public function getSubCategoriesAttribute($value)
    {
        return explode(',', $value);
    }
    public function getAvgRatingAttribute()
    {

        $revData = BookingReview::where('shop_id', $this->attributes['id'])->get();
        $revDataP = ProductReview::where('shop_id', $this->attributes['id'])->get();
        $star = $revData->sum('star');
        $star += $revDataP->sum('star');
        if ($star > 1) {
            $c = count($revData) + count($revDataP);
            $t = $star / $c;
            return number_format($t, 1, '.', '');
        }
        return 0;
        // return rand(0, 5);
    }
    public function getServiceDataAttribute()
    {
        return OwnerService::whereIn('id', explode(',', $this->attributes['services']))->get();
    }
    public function getEmployeeDataAttribute()
    {
        return Employee::whereIn('id', explode(',', $this->attributes['employee']))->get(['name', 'id', 'email', 'online', 'image', 'start_time', 'end_time']);
    }
    public function getProductDataAttribute()
    {
        return OwnerProduct::whereIn('id', explode(',', $this->attributes['products']))->get(['name', 'id', 'detail', 'price', 'status']);
    }
    public function getPackageDataAttribute()
    {
        return OwnerPackage::whereIn('id', explode(',', $this->attributes['packages']))->get();
    }
    public function getManagerDataAttribute()
    {
        return OwnerManager::whereIn('id', explode(',', $this->attributes['managers']))->get();
    }
}
