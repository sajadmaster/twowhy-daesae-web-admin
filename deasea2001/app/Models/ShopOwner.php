<?php

namespace App\Models;

use Bavix\Wallet\Interfaces\Wallet;
use Bavix\Wallet\Traits\HasWallet;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class ShopOwner extends Authenticatable implements Wallet
{
    use HasFactory, HasApiTokens, Notifiable, HasWallet;
    protected $fillable = [
        'name', 'email', 'phone_no', 'otp', 'status', 'image', 'password', 'device_token', 'noti', 'verified', 'is_daesea', 'provider', 'provider_token'
    ];
    protected $table = 'shop_owner';
    protected $hidden = [
        'password', 'created_at', 'updated_at', 'otp'
    ];
    protected $appends = ['imageUri'];
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['image'])) {

            return url('upload/') . '/' . $this->attributes['image'];
        }
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function Shop()
    {
        return $this->hasMany('App\Models\OwnerShop', 'owner_id', 'id');
    }
}

