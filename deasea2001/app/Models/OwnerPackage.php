<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerPackage extends Model
{
    use HasFactory;
    protected $fillable = [
        'owner_id', 'name', 'detail', 'discount_price', 'products', 'status', 'price'
    ];
    protected $table = 'owner_package';
    public function Images()
    {
        return $this->hasMany('App\Models\PackageImage', 'package_id', 'id');
    }
    public function getProductsAttribute($value)
    {
        return explode(',', $value);
    }
    public function getProductDataAttribute()
    {
        if (isset($this->attributes['products'])) {
            return OwnerProduct::whereIn('id', explode(',', $this->attributes['products']))->get(['id', 'name']);
        }
        return  [];
    }
}
