<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    use HasFactory;
    public $table = 'inquiry';

    protected $dates = [
        'created_at',
        'updated_at',

    ];

    protected $fillable = [
        'user_id', 'subject', 'details'
    ];

    public function User()
    {
        return $this->belongsTo('App\Models\AppUsers', 'user_id', 'id');
    }
}
