<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class Employee extends Authenticatable
{
    use HasFactory, HasApiTokens, Notifiable;
    protected $fillable = [
        'owner_id', 'email', 'phone_no', 'name', 'status', 'image', 'password', 'device_token', 'noti', 'start_time', 'end_time', 'experience', 'id_no', 'online', 'otp', 'regions', 'lat', 'lng'
    ];
    protected $table = 'employee';
    protected $hidden = [
        'password', 'created_at', 'updated_at',
    ];
    protected $appends = ['imageUri', 'avgRating'];

    public function getAvgRatingAttribute()
    {
        $bids = BookingMaster::where('employee_id', $this->attributes['id'])->get()->pluck('id');
        $pids = ProductOrder::where('employee_id', $this->attributes['id'])->get()->pluck('id');
        $revData = BookingReview::whereIn('order_id', $bids)->get();
        $revDataP = ProductReview::whereIn('order_id', $pids)->get();

        $star = $revData->sum('star');
        $star += $revDataP->sum('star');
        if ($star > 1) {
            $c = count($revData) + count($revDataP);
            $t = $star / $c;
            return number_format($t, 1, '.', '');
        }
        return 0;
    }
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['image'])) {

            return url('upload/') . '/' . $this->attributes['image'];
        }
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function Reviews()
    {
        return $this->hasMany('App\Review', 'employee_id', 'id')->orderBy('created_at', 'desc');
    }
    public function Booking()
    {
        return $this->hasMany('App\Models\BookingMaster', 'employee_id', 'id')->orderBy('created_at', 'desc');
    }
    public function getRegionsAttribute($value)
    {
        return explode(',', $value);
    }
}
