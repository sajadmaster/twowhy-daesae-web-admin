<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class OwnerManager extends Authenticatable
{
    use HasFactory, HasApiTokens, Notifiable;
    protected $fillable = [
        'name', 'email', 'phone_no', 'status', 'image', 'password', 'device_token', 'noti', 'verified', 'owner_id', 'regions'
    ];
    protected $table = 'owner_manager';
    protected $hidden = [
        'password', 'created_at', 'updated_at', 'otp'
    ];
    protected $appends = ['imageUri'];
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['image'])) {

            return url('upload/') . '/' . $this->attributes['image'];
        }
    }
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
    public function getRegionsAttribute($value)
    {
        return explode(',', $value);
    }
}
