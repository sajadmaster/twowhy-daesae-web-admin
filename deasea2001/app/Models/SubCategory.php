<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;
    public $table = 'sub_categories';

    protected $dates = [
        'created_at',
        'updated_at',

    ];

    protected $fillable = [
        'name', 'icon', 'status', 'cat_id'
    ];
    protected $hidden = [
        'created_at',
        'updated_at',

    ];
    protected $appends = ['imageUri'];
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['icon'])) {

            return url('upload/') . '/' . $this->attributes['icon'];
        }
    }
    public function Category()
    {
        return $this->belongsTo('App\Models\Category', 'cat_id', 'id');
    }
}
