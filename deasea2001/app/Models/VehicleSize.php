<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleSize extends Model
{
    use HasFactory;

    public $table = 'vehicle_size';

    protected $dates = [
        'created_at',
        'updated_at',

    ];

    protected $fillable = [
        'name', 'status'
    ];
    protected $hidden = [
        'created_at',
        'updated_at',

    ];

}
