<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BookingMaster extends Model
{
    use HasFactory;
    protected $fillable = [
        'booking_id', 'user_id', 'shop_id', 'owner_id', 'employee_id', 'admin_per', 'address', 'vehicle_id', 'service_type', 'amount', 'discount', 'payment_status', 'payment_token', 'payment_method', 'status', 'region', 'lat', 'lng'
    ];
    public $table = 'booking_master';
    protected $dates = [
        'updated_at',
        'created_at',
    ];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('latest', function (Builder $builder) {
            $builder->orderby('created_at', 'desc');
        });
    }
    public function Shop()
    {
        return $this->belongsTo('App\Models\OwnerShop', 'shop_id', 'id');
    }
    public function User()
    {
        return $this->belongsTo('App\Models\AppUsers', 'user_id', 'id');
    }
    public function Employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }
    public function Address()
    {
        return $this->belongsTo('App\Models\UserAddress', 'address', 'id');
    }
    public function Vehicle()
    {
        return $this->belongsTo('App\Models\UserVehicle', 'vehicle_id', 'id');
    }
    public function Item()
    {
        return $this->hasMany('App\Models\BookingChild', 'master_id', 'id');
    }
    public function Review()
    {
        return $this->hasOne('App\Models\BookingReview', 'order_id', 'id');
    }
    public function scopeGetByDistance($query, $lat, $lang, $radius)
    {

        // dd($radius);
        $radius = AdminSetting::first()->radius;
        $results = DB::select(DB::raw('SELECT id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lang . ') ) + sin( radians(' . $lat . ') ) * sin( radians(lat) ) ) ) AS distance FROM booking_master HAVING distance < ' . $radius . ' ORDER BY distance'));
        // dd($results);
        if (!empty($results)) {

            $ids = [];

            //Extract the id's
            foreach ($results as $q) {
                array_push($ids, $q->id);
            }
            return $query->whereIn('id', $ids);
        }
        return $query->whereIn('id', []);
    }
}
