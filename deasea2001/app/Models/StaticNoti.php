<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaticNoti extends Model
{
    use HasFactory;
    public $table = 'static_notification';

    protected $dates = [
        'created_at',
        'updated_at',

    ];

    protected $fillable = [
        'for_what', 'title', 'sub_title', 'status', 'for_who'
    ];
}
