<?php

namespace App\Http\Controllers;

use App\Models\AppUsers;
use App\Models\BookingMaster;
use App\Models\Category;
use App\Models\Inquiry;
use App\Models\ProductOrder;
use App\Models\Region;
use App\Models\ShopOwner;
use App\Models\SubCategory;
use App\Models\Tip;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use App\Models\WithdrawalApplication;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $master = array();
        $master['customer'] = AppUsers::all()->count();
        $master['owner_all'] = ShopOwner::all()->count();
        $master['daesea'] = ShopOwner::where('is_daesea', 1)->get()->count();
        $master['category'] = Category::all()->count();
        $master['sub_category'] = SubCategory::all()->count();
        $master['brand'] = VehicleBrand::all()->count();
        $master['model'] = VehicleModel::all()->count();
        $master['region'] = Region::all()->count();
        $master['inquiry'] = Inquiry::all()->count();
        $master['withdrawal'] = WithdrawalApplication::all()->count();
        $master['tip'] = Tip::all()->count();

        $monthDate = Carbon::now()->subMonths(11);
        // dd(Carbon::now()->startOfMonth()->toDateTimeString(), Carbon::now()->endOfMonth()->toDateTimeString());
        //
        // 
        $product = array();
        $counting = array();
        $monthName = array();
        for ($i = 0; $i < 12; $i++) {
            # code...
            $start = $monthDate->copy()->startOfMonth()->toDateTimeString();
            $end = $monthDate->copy()->endOfMonth()->toDateTimeString();
            $count = BookingMaster::whereBetween('created_at', array($start, $end))->count();
            $payment = ProductOrder::whereBetween('created_at', array($start, $end))->count();

            array_push($product, $payment);
            array_push($counting, $count);
            array_push($monthName, $monthDate->format('M'));
            $monthDate->addMonth();
        }
        $master['orderCount'] = json_encode($product);
        $master['bookingCount'] =  json_encode($counting);
        $master['monthName'] = json_encode($monthName);

        return view('dashboard', compact('master'));
    }
}
