<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordRequest;
use App\Models\AdminSetting;
use App\Models\AppUsers;
use App\Models\BookingReview;
use App\Models\Notifications;
use App\Models\OwnerShop;
use App\Models\ProductReview;
use App\Models\ShopOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AppUsersController extends Controller
{
    //
    public function testingKakao(Request $request)
    {

        $code = $request->code;
        $tokns = $this->kakaoStepOne($code);
        $t = json_decode($tokns, true);
        $tokns = $this->kakaoStepTwo($t['access_token']);
        $t = json_decode($tokns, true);
        $user = AppUsers::firstOrCreate([
            'provider' => "KAKAO",
            'provider_token' => $t['id'],
        ], [
            'provider' => "KAKAO",
            'provider_token' => $t['id'],
            'email' => $t['kakao_account']['email'],
            'name' => $t['kakao_account']['profile']['nickname'],
        ]);
        return redirect()->route('kakaoBack', ['id' => $t['id']]);
        return "please go to app bake";
    }
    public function testingKakaoOwner(Request $request)
    {

        $code = $request->code;
        $tokns = $this->kakaoStepOneOwner($code);
        $t = json_decode($tokns, true);
        $tokns = $this->kakaoStepTwo($t['access_token']);
        $t = json_decode($tokns, true);
        $user = ShopOwner::firstOrCreate([
            'provider' => "KAKAO",
            'provider_token' => $t['id'],
        ], [
            'provider' => "KAKAO",
            'provider_token' => $t['id'],
            'email' => $t['kakao_account']['email'],
            'name' => $t['kakao_account']['profile']['nickname'],
        ]);
        return redirect()->route('kakaoBack', ['id' => $t['id']]);
        return "please go to app bake";
    }
    public function backToApp($id)
    {
        return $id . "please go back to app dear";
    }
    public function kakaoStepOne($code)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://kauth.kakao.com/oauth/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => "grant_type=authorization_code&client_id=ca7b63405bceaa810bbc50806dee8ac0&redirect_uri=https://daesae.com/user/kakao&code=$code",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
        echo $response;

    }
    public function kakaoStepOneOwner($code)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://kauth.kakao.com/oauth/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => "grant_type=authorization_code&client_id=ca7b63405bceaa810bbc50806dee8ac0&redirect_uri=https://daesae.com/user/owner/kakao&code=$code",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
        echo $response;

    }
    public function kakaoStepTwo($access)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://kapi.kakao.com/v2/user/me',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $access",
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
        echo $response;

    }
    public function testingNaver(Request $request)
    {

        $code = $request->code;
        $state = $request->state;
        $tokns = $this->naverStepOne($code);
        $t = json_decode($tokns, true);

        $profileData = $this->getProfileData($t['access_token']);
        $t = json_decode($profileData, true);

     
        $id = $this->saveNaverData($t['response'], $state);

        return redirect()->route('kakaoBack', ['id' => $id]);

    }
    public function naverStepOne($code)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://nid.naver.com/oauth2.0/token?client_id=_TVy6aXvMB6uFeUdTjsl&client_secret=cKnSqXOrBF&grant_type=authorization_code&code=$code",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
    public function getProfileData($token)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://openapi.naver.com/v1/nid/me',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $token",
                'User-Agent: curl/7.12.1 (i686-redhat-linux-gnu) libcurl/7.12.1 OpenSSL/0.9.7a zlib/1.2.1.2 libidn/0.5.6',
                'Host: openapi.naver.com',
                'Pragma: no-cache',
                'Accept: */*',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;
        return $response;
    }

    public function saveNaverData($t, $type)
    {
        if ($type == "user") {
            $user = AppUsers::firstOrCreate([
                'provider' => "NAVER",
                'provider_token' => $t['id'],
            ], [
                'provider' => "NAVER",
                'provider_token' => $t['id'],
                'email' => $t['email'],
                'name' => $t['name'],
            ]);

        } else {
            $user = ShopOwner::firstOrCreate([
                'provider' => "NAVER",
                'provider_token' => $t['id'],
            ], [
                'provider' => "NAVER",
                'provider_token' => $t['id'],
                'email' => $t['email'],
                'name' => $t['name'],
            ]);

        }
        return $t['id'];
    }
    public function tetstagain()
    {
        $user = AppUsers::find(1);
        $owner = ShopOwner::find(1);
        $user->transfer($owner, 500);
        // $user->deposit(10, ['type' => "der"]);
        // dd($user->transactions()->get());
        return [$user->balance, $owner->balance];
    }
    public function index()
    {
        //

        $appuser = AppUsers::all();
        return view('admin.appuser.index', compact('appuser'));
    }
    public function privacy()
    {
        $d = AdminSetting::first();
        return response()->json(['msg' => null, 'data' => $d->pp, 'success' => true], 200);
    }
    public function aboutus()
    {
        $d = AdminSetting::first(['email', 'address', 'phone_no', 'ios_version', 'android_version']);
        return response()->json(['msg' => null, 'data' => $d, 'success' => true], 200);
    }
    public function setting()
    {
        $d = AdminSetting::first(['notification', 'free_point', 'paypal_status']);
        $d['paypal_key'] = env('PAYPAL_KEY');
        $d['app_id'] = env('APP_ID');
        $d['rest_api_key'] = env('REST_API_KEY');
        $d['user_auth_key'] = env('USER_AUTH_KEY');
        $d['project_number'] = env('PROJECT_NUMBER');
        return response()->json(['msg' => null, 'data' => $d, 'success' => true], 200);
    }
    public function changeStatus($id)
    {

        $data = AppUsers::findOrFail($id);
        $data->status = $data->status === 1 ? 0 : 1;
        $data->update();
        return redirect()->back()->withStatus(__('Status Is changed.'));
    }
    public function login(Request $request)
    {
        //

        $user = AppUsers::where('email', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            // if ($user['verified'] != 1) {
            //     return response()->json(['msg' => 'Please Verify your account', 'data' => null, 'success' => false, 'verification' => true], 200);
            // }
            if ($user['status'] == 0) {
                return response()->json(['msg' => 'You are block by admin', 'data' => null, 'success' => false], 200);
            }
            $token = $user->createToken('user')->accessToken;
            $user['device_token'] = $request->device_token;
            $user->save();
            $user['token'] = $token;
            return response()->json(['msg' => 'Welcome back  ', 'data' => $user, 'success' => true], 200);
        } else {
            return response()->json(['msg' => 'Email and Password not match with our record', 'data' => null, 'success' => false], 200);
        }
    }
    public function socialLogin(Request $request)
    {
        //

        $user = AppUsers::firstOrCreate([
            'provider' => $request->provider,
            'provider_token' => $request->provider_token,
        ], $request->all());
        if ($user) {
            // if ($user['verified'] != 1) {
            //     return response()->json(['msg' => 'Please Verify your account', 'data' => null, 'success' => false, 'verification' => true], 200);
            // }
            if ($user['status'] == 0) {
                return response()->json(['msg' => 'You are block by admin', 'data' => null, 'success' => false], 200);
            }
            $token = $user->createToken('user')->accessToken;
            $user['device_token'] = $request->device_token;
            $user->save();
            $user['token'] = $token;
            return response()->json(['msg' => 'Welcome back  ', 'data' => $user, 'success' => true], 200);
        } else {
            return response()->json(['msg' => 'Email and Password not match with our record', 'data' => null, 'success' => false], 200);
        }
    }
    public function socialLoginOwner(Request $request)
    {
        //

        $user = ShopOwner::firstOrCreate([
            'provider' => $request->provider,
            'provider_token' => $request->provider_token,
        ], $request->all());
        if ($user) {
            // if ($user['verified'] != 1) {
            //     return response()->json(['msg' => 'Please Verify your account', 'data' => null, 'success' => false, 'verification' => true], 200);
            // }
            if ($user['status'] == 0) {
                return response()->json(['msg' => 'You are block by admin', 'data' => null, 'success' => false], 200);
            }
            $token = $user->createToken('user')->accessToken;
            $user['device_token'] = $request->device_token;
            $user->save();
            $user['token'] = $token;
            return response()->json(['msg' => 'Welcome back  ', 'data' => $user, 'success' => true], 200);
        } else {
            return response()->json(['msg' => 'Email and Password not match with our record', 'data' => null, 'success' => false], 200);
        }
    }
    public function store(Request $request)
    {
        //
        $request->validate([
            'email' => 'bail|required|email|unique:app_users,email',
            'name' => 'bail|required',
            'password' => 'bail|required|min:6',
            'phone_no' => 'bail|required|unique:app_users,phone_no',
        ]);
        $reqData = $request->all();

        $app = AdminSetting::get(['id', 'verification', 'sms_gateway'])->first();
        $flow = $app->verification == 1 ? 'verification' : 'home';
        if ($app->verification != 1) {
            $reqData['verified'] = 1;
        } else {
            // try {
            //     $res = (new Admin\TwilioController)->sendOTPUser($request, $app->sms_gateway, 'verification', 0);
            //     if ($res['success'] === true) {
            //         $reqData['otp'] = $res['otp'];
            //         // $reqData['otp'] = '0000';
            //     }
            // } catch (\Exception $e) {
            //     $reqData['verified'] = 1;
            //     $reqData['otp'] = '0000';
            //     //  dd($e->getMessage());
            // }
        }

        $data = AppUsers::create($reqData);
        // if ($app->verification != 1) {
        $token = $data->createToken('user')->accessToken;
        $data['token'] = $token;
        // }
        return response()->json(['msg' => 'Welcome...', 'data' => $data, 'success' => true, 'flow' => $flow], 200);
    }
    public function password(PasswordRequest $request)
    {

        auth()->user()->update(['password' => $request->get('password')]);
        $data['token'] = auth()->user()->createToken('user')->accessToken;
        return response()->json(['msg' => "Password Change", 'data' => $data['token'], 'success' => true], 200);
    }
    public function profilePictureUpdate(Request $request)
    {
        $name = (new AppHelper)->saveBase64($request->image);

        auth()->user()->update([
            'image' => $name,
        ]);
        return response()->json(['msg' => 'Profile Updated', 'data' => null, 'success' => true], 200);
    }
    public function profile(Request $request)
    {

        return response()->json(['msg' => 'Profile Updated', 'data' => auth()->user(), 'success' => true], 200);
    }
    public function profileUpdate(Request $request)
    {

        auth()->user()->update($request->all());
        return response()->json(['msg' => 'Profile Updated', 'data' => null, 'success' => true], 200);
    }
    public function reviewList()
    {
        $data = ProductReview::with('images:id,image,review_id')->where('user_id', Auth::id())->orderBy('created_at', 'desc')->get();
        $Sdata = BookingReview::with('images:id,image,review_id')->where('user_id', Auth::id())->orderBy('created_at', 'desc')->get();
        $data = array_merge($data->toArray(), $Sdata->toArray());
        $data = collect($data);
        $sorted = $data->sortByDesc('created_at');
        return response()->json(['msg' => null, 'data' => $sorted->values()->all(), 'success' => true], 200);
    }

    public function reviewListOwner()
    {
        if (Auth::getDefaultDriver() == "manager") {
            $ids = (new AppHelper)->managerShop();
        } else {
            $ids = OwnerShop::where('owner_id', Auth::id())->get(['id'])->pluck('id');
        }
        $data = ProductReview::with(['images:id,image,review_id', 'shop:id,name'])->whereIn('shop_id', $ids)->orderBy('created_at', 'desc')->get();
        $Sdata = BookingReview::with(['images:id,image,review_id', 'shop:id,name'])->whereIn('shop_id', $ids)->orderBy('created_at', 'desc')->get();
        $d['booking'] = $Sdata;
        $d['product'] = $data;
        return response()->json(['msg' => null, 'data' => $d, 'success' => true], 200);
    }
    public function notificationUser()
    {
        $d = Notifications::where('user_id', Auth::id())->latest()->get();
        return response()->json(['msg' => null, 'data' => $d, 'success' => true], 200);
    }
    public function notificationOwner()
    {
        $d = Notifications::where('owner_id', Auth::id())->latest()->get();
        return response()->json(['msg' => null, 'data' => $d, 'success' => true], 200);
    }
    public function notificationEmployee()
    {
        $d = Notifications::where('emp_id', Auth::id())->latest()->get();
        return response()->json(['msg' => null, 'data' => $d, 'success' => true], 200);
    }
}
