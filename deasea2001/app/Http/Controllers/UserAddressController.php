<?php

namespace App\Http\Controllers;

use App\Models\AdminSetting;
use App\Models\AppUsers;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =  UserAddress::where('user_id', Auth::id())->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'address_name' => 'bail|required',
            'depth_1' => 'bail|required',
            'depth_2' => 'bail|required',
            'depth_3' => 'bail|required',
            'main_address_no' => 'bail|required',
            'lat' => 'bail|required',
            'lang' => 'bail|required',
            'type' => 'bail|required'
        ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();
        if (isset($request->image)) {
            $name = (new AppHelper)->saveBase64($request->image);
            $reqData['image'] = $name;
        }
        UserAddress::create($reqData);
        $set  = AdminSetting::first();
        if ($set->free_point == 1) {
            $user = AppUsers::find(Auth::id());
            $user->deposit($set->address_point, ['payment_token' => "Using Give Address"]);
        }
        return response()->json(['msg' => "Address added successfully", 'data' => null, 'success' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserAddress  $userAddress
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data =  UserAddress::find($id);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserAddress  $userAddress
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserAddress  $userAddress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserAddress  $userAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
