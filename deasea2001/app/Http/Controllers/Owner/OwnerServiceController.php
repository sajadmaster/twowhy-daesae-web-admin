<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;

use App\Models\OwnerService;
use App\Models\ServiceImage;
use App\Models\ServicePrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OwnerServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //OwnerService
        $data = OwnerService::with(['images:image,id,service_id', 'category:id,name', 'SubCategory:id,name'])->where('owner_id', Auth::id())->get(['id', 'name', 'detail', 'cat_id', 'sub_cat_id']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'bail|required',
            'detail' => 'bail|required',
            'cat_id' => 'bail|required',
            'sub_cat_id' => 'bail|required',
            'sell_at' => 'bail|required',


        ]);
        $reqData = $request->all();
        $reqData['owner_id'] = Auth::id();
        $data = OwnerService::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                ServiceImage::create([
                    "service_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }
        if ($request->has('prices')) {

            $prices =  json_decode($request->prices, true);
            foreach ($prices as  $value) {

                ServicePrice::create([
                    "service_id" => $data->id,
                    "size_id" => $value['size_id'],
                    "in_price" => $value['in_price'],
                    "out_price" => $value['out_price'],
                ]);
            }
        }
        return response()->json(['msg' => 'Service added successfully.', 'data' => null, 'success' => true], 201);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OwnerService  $ownerService
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = OwnerService::find($id);
        $data->load(['images:image,id,service_id', 'prices', 'category:id,name', 'SubCategory:id,name']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OwnerService  $ownerService
     * @return \Illuminate\Http\Response
     */
    public function edit(OwnerService $ownerService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OwnerService  $ownerService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = OwnerService::find($id);
        if ($request->has('prices')) {

            $prices =  json_decode($request->prices, true);
            foreach ($prices as  $value) {

                ServicePrice::updateOrCreate(
                    ["service_id" => $data->id],
                    [
                        "service_id" => $data->id,
                        "size_id" => $value['size_id'],
                        "in_price" => $value['in_price'],
                        "out_price" => $value['out_price'],
                    ]
                );
            }
        }
        $data->update($request->all());
        return response()->json(['msg' => 'Service updated successfully.', 'data' => null, 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OwnerService  $ownerService
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        ServiceImage::where('service_id', $id)->delete();
        ServicePrice::where('service_id', $id)->delete();
        $data = OwnerService::find($id)->delete();
        return response()->json(['msg' => 'Service deleted successfully.', 'data' => null, 'success' => true], 200);
    }
    public function deleteImage($id)
    {
        ServiceImage::find($id)->delete();
        return response()->json(['msg' => 'Service image deleted successfully.', 'data' => null, 'success' => true], 200);
    }
    public function addImage($id, Request $request)
    {
        $i = (new AppHelper)->saveBase64($request->image);

        ServiceImage::create([
            "service_id" => $id,
            "image" => $i,
        ]);
        return response()->json(['msg' => 'Service image added successfully.', 'data' => null, 'success' => true], 201);
    }
    public function newPrice(Request $request)
    {
        $reqData = $request->all();
        ServicePrice::updateOrCreate(
            ['service_id' => $reqData['service_id'], 'size_id' => $reqData['size_id']],
            $reqData
        );
        return response()->json(['msg' => 'Service price updated successfully.', 'data' => null, 'success' => true], 200);
    }
    public function deletePrice($id)
    {
        ServicePrice::find($id)->delete();
        return response()->json(['msg' => 'Service price deleted successfully.', 'data' => null, 'success' => true], 200);
    }
}
