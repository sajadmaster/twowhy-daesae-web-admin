<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;

use App\Models\OwnerPackage;
use App\Models\PackageImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OwnerPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = OwnerPackage::with(['images:id,image,package_id'])->where('owner_id', Auth::id())->get()->each->setAppends(['productData']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required',
            'detail' => 'bail|required',
            'products' => 'bail|required',

            'price' => 'bail|required',
            'status' => 'bail|required',

        ]);
        $reqData = $request->all();
        $reqData['owner_id'] = Auth::id();
        $data = OwnerPackage::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                PackageImage::create([
                    "package_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }

        return response()->json(['msg' => 'Package added successfully.', 'data' => null, 'success' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OwnerPackage  $ownerPackage
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = OwnerPackage::find($id);
        $data->load(['images:image,id,package_id']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OwnerPackage  $ownerPackage
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OwnerPackage  $ownerPackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = OwnerPackage::find($id);
        $data->update($request->all());
        return response()->json(['msg' => 'Package updated successfully.', 'data' => null, 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OwnerPackage  $ownerPackage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PackageImage::where('package_id', $id)->delete();
        $data = OwnerPackage::find($id)->delete();
        return response()->json(['msg' => 'Package deleted successfully.', 'data' => null, 'success' => true], 200);
        //
    }

    public function deleteImage($id)
    {
        PackageImage::find($id)->delete();
        return response()->json(['msg' => 'Package image deleted successfully.', 'data' => null, 'success' => true], 200);
    }
    public function addImage($id, Request $request)
    {
        $i = (new AppHelper)->saveBase64($request->image);

        PackageImage::create([
            "package_id" => $id,
            "image" => $i,
        ]);
        return response()->json(['msg' => 'Package image added successfully.', 'data' => null, 'success' => true], 201);
    }
}
