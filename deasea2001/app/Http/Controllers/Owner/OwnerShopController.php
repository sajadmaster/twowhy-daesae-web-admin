<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\OwnerPackage;
use App\Models\OwnerProduct;
use App\Models\OwnerService;
use App\Models\OwnerShop;
use App\Models\ShopOwner;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OwnerShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::getDefaultDriver() == "manager") {
            $ids = (new AppHelper)->managerShop();
            $data = OwnerShop::whereIn('id',  $ids)->get();
        } else {
            // return "dert";
            $data = OwnerShop::where('owner_id', Auth::id())->get();
        }
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //     
        $request->validate([
            'name' => 'bail|required',
            'address' => 'bail|required',
            'phone_no' => 'bail|required',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required'
        ]);
        $reqData = $request->all();
        $reqData['owner_id']  = Auth::id();
        if (isset($request->image)) {
            $reqData['image'] = (new AppHelper)->saveBase64($request->image);
        }
        OwnerShop::create($reqData);
        return response()->json(['msg' => 'Shop Addedd successfully.', 'data' => null, 'success' => true], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OwnerShop  $ownerShop
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = OwnerShop::find($id);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OwnerShop  $ownerShop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OwnerShop  $ownerShop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'bail|required',
            'address' => 'bail|required',
            'phone_no' => 'bail|required',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required'
        ]);
        $reqData = $request->all();
        $reqData['owner_id']  = Auth::id();
        if (isset($request->image)) {
            $reqData['image'] = (new AppHelper)->saveBase64($request->image);
        }
        OwnerShop::find($id)->update($reqData);
        return response()->json(['msg' => 'Shop Addedd successfully.', 'data' => null, 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OwnerShop  $ownerShop
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = OwnerShop::find($id)->delete();
return response()->json(['msg' => 'Service deleted successfully.', 'data' => null, 'success' => true], 200);


        //
    }
    public function shopByRegion($id)
    {

        $ids = ShopOwner::where([['is_daesea', 1], ['status', 1]])->get('id')->pluck('id');
        $data = OwnerShop::whereIn('owner_id', $ids)->where(function (Builder $query) use ($id) {
            return $query->whereRaw('FIND_IN_SET(?, regions)', [$id]);
        })->get(['id', 'name', 'address', 'phone_no', 'image', 'start_time', 'end_time', 'owner_id']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function shopForProduct(Request $request)
    {

        $ids = ShopOwner::where([['status', 1], ['verified', 1]])->get('id')->pluck('id');
        $data = OwnerShop::whereIn('owner_id', $ids)->where(function (Builder $query) use ($request) {
            return $query->whereRaw('FIND_IN_SET(?, regions)', [$request->regions])->whereRaw('FIND_IN_SET(?, sub_categories)', [$request->sub]);
        })->get(['id', 'name', 'address', 'phone_no', 'image', 'start_time', 'end_time']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function shopService($id)
    {
        $data = OwnerShop::find($id);
        // 

        $service =  OwnerService::with([
            'prices:service_id,id,out_price,in_price,size_id',
            'prices.size' => function ($query) {
                $query->where('status', 1)->select('id', 'name', 'status');
            }
        ])->where('status', 1)->whereIn('id', $data->services)->get(['name', 'detail', 'discount', 'id']);
        return response()->json(['msg' => null, 'data' => $service, 'success' => true], 200);
    }
    public function shopProduct($id)
    {
        $data = OwnerShop::find($id);
        $data->load('productReview.images');
        $data['product'] = OwnerProduct::with(['category:id,name', 'images:id,image'])->whereIn('id', $data->products)->where('status', 1)->get(['name', 'id', 'cat_id', 'price']);
        // review baki
        // cat_id price
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function productView($id)
    {
        $data =   OwnerProduct::with(['category:id,name', 'images:id,image'])->where([['status', 1], ['id', $id]])->first(['name', 'id', 'cat_id', 'price', 'detail']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function getShopEmployee($id)
    {
        $data = OwnerShop::find($id);
        $e =  Employee::whereIn('id', $data->employee)->where('status', 1)->get(['id', 'name']);
        return response()->json(['msg' => null, 'data' => $e, 'success' => true], 200);
    }
}
