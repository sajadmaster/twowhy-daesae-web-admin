<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use Illuminate\Http\Request;

class VehicleModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $vehicleModel = VehicleModel::with('brand:id,name')->get();

        return view('admin.vehicleModel.index', compact('vehicleModel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $brand = VehicleBrand::orderBy('name', 'asc')->get();
        return view('admin.vehicleModel.create', compact('brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required|',

        ]);
        $reqData = $request->all();

        VehicleModel::create($reqData);
        return redirect()->route('vehicle-model.index')->withStatus(__('vehicleModel is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VehicleModel  $vehicleModel
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleModel $vehicleModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VehicleModel  $vehicleModel
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleModel $vehicleModel)
    {
        //
        $brand = VehicleBrand::orderBy('name', 'asc')->get();

        return view('admin.vehicleModel.edit', compact('vehicleModel', 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VehicleModel  $vehicleModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleModel $vehicleModel)
    {
        //
        $request->validate([
            'name' => 'bail|required|max:255',
        ]);
        $reqData = $request->all();

        $vehicleModel->update($reqData);

        return redirect()->route('vehicle-model.index')->withStatus(__('vehicleModel is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VehicleModel  $vehicleModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleModel $vehicleModel)
    {
        //
        $vehicleModel->delete();

        return back()->withStatus(__('vehicle model is deleted successfully.'));
    }
}
