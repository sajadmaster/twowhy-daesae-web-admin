<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\Tip;
use Illuminate\Http\Request;

class TipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tips = Tip::get();

        return view('admin.tip.index', compact('tips'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.tip.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'bail|required|max:255',
            'detail' => 'bail|required',
            'tag' => 'bail|required',
        ]);
        $reqData = $request->all();

        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }

        $reqData['status'] = $request->has('status') ? 1 : 0;
        Tip::create($reqData);
        return redirect()->route('tip.index')->withStatus(__('Tip is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tip  $tip
     * @return \Illuminate\Http\Response
     */
    public function show(Tip $tip)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tip  $tip
     * @return \Illuminate\Http\Response
     */
    public function edit(Tip $tip)
    {
        //
        return view('admin.tip.edit', compact('tip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tip  $tip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tip $tip)
    {
        //
        $request->validate([
            'title' => 'bail|required|max:255',
            'detail' => 'bail|required',
            'tag' => 'bail|required',
        ]);
        $reqData = $request->all();

        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }

        $reqData['status'] = $request->has('status') ? 1 : 0;


        $tip->update($reqData);

        return redirect()->route('tip.index')->withStatus(__('Tip is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tip  $tip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tip $tip)
    {
        //
        $tip->delete();

        return back()->withStatus(__('tip is deleted successfully.'));
    }

    public function apiIndex()
    {
        $tip = Tip::where('status', 1)->latest()->get();
        return response()->json(['msg' => null, 'data' => $tip, 'success' => true], 200);
    }
    public function singleTip($id)
    {
        $tip = Tip::find($id);
        $tip->increment('views', 1);
        return response()->json(['msg' => null, 'data' => $tip, 'success' => true], 200);
    }
}
