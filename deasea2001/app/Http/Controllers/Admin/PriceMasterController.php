<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PointPackage;
use App\Models\PriceChild;
use App\Models\PriceMaster;
use App\Models\VehicleSize;
use Illuminate\Http\Request;

class PriceMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $priceMaster = PriceMaster::all();

        return view('admin.priceMaster.index', compact('priceMaster'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $size = VehicleSize::where('status', 1)->get();
        return view('admin.priceMaster.create', compact('size'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $request->validate([
            'name' => 'bail|required',

            'in_count' => 'bail|required|numeric',
            'out_count' => 'bail|required|numeric',

        ]);
        $reqData = $request->all();
        $reqData['type'] = $request->has('type') ? 1 : 0;
        $data = PriceMaster::create($reqData);

        for ($i = 0; $i < count($reqData['size_id']); $i++) {
            $temp['master_id'] = $data['id'];
            if ($reqData['type'] == 0) {
                $temp['out_price'] = $reqData['out_price'][$i];
                $temp['in_price'] = $reqData['in_price'][$i];
                // diff
            } else {
                $temp['mega_price'] = $reqData['mega_price'][$i];
                //mega
            }
            $temp['size_id'] = $reqData['size_id'][$i];

            PriceChild::create($temp);
        }

        return redirect()->route('price-master.index')->withStatus(__('Price Master is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PriceMaster  $priceMaster
     * @return \Illuminate\Http\Response
     */
    public function show(PriceMaster $priceMaster)
    {
        //start_time
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PriceMaster  $priceMaster
     * @return \Illuminate\Http\Response
     */
    public function edit(PriceMaster $priceMaster)
    {
        //
        $size = VehicleSize::where('status', 1)->get();
        foreach ($size as  $value) {
            $pc = PriceChild::where([['master_id', $priceMaster->id], ['size_id', $value['id']]])->first();
            $value['in_price'] = $pc->in_price;
            $value['mega_price'] = $pc->mega_price;
            $value['out_price'] = $pc->out_price;
        }
        return view('admin.priceMaster.edit', compact('priceMaster', 'size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PriceMaster  $priceMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PriceMaster $priceMaster)
    {
        //
        $request->validate([
            'name' => 'bail|required',
            'in_count' => 'bail|required|numeric',
            'out_count' => 'bail|required|numeric',

        ]);
        $reqData = $request->all();
        $reqData['type'] = $request->has('type') ? 1 : 0;

        $priceMaster->update($reqData);
        for ($i = 0; $i < count($reqData['size_id']); $i++) {
            $temp['master_id'] = $priceMaster['id'];
            if ($reqData['type'] == 0) {
                $temp['out_price'] = $reqData['out_price'][$i];
                $temp['in_price'] = $reqData['in_price'][$i];
                // diff
            } else {
                $temp['mega_price'] = $reqData['mega_price'][$i];
                //mega
            }
            $temp['size_id'] = $reqData['size_id'][$i];

            PriceChild::updateOrCreate(
                ['size_id' => $reqData['size_id'][$i]],
                $temp
            );
        }
        return redirect()->route('price-master.index')->withStatus(__('Price Master is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PriceMaster  $priceMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(PriceMaster $priceMaster)
    {
        //
        PriceChild::where('master_id', $priceMaster->id)->delete();
        $priceMaster->delete();

        return back()->withStatus(__('Price Master is deleted successfully.'));
    }


    public function apiIndex()
    {
        $sizes =  VehicleSize::get();
        foreach ($sizes as  $size) {
            $size['prices'] = PriceChild::where('size_id', $size->id)->orderBy('master_id', 'asc')->get();
        }
        $data['size'] = $sizes;
        $data['package'] = PriceMaster::orderBy('id', 'asc')->get();
        // $data = PriceMaster::with(['child', 'child.size:name,id'])->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function singlePrice(Request $request)
    {

        $data = PriceChild::where([['master_id', $request->id], ['size_id', $request->size_id]])->first();
        $data['type'] = PriceMaster::find($request->id)->type;
        // $data = PriceMaster::with(['child', 'child.size:name,id'])->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
}
