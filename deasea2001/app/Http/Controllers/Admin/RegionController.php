<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $region = Region::all();

        return view('admin.region.index', compact('region'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.region.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required|unique:regions|max:50',
            'lat' => 'bail|required',
            'lang' => 'bail|required',

        ]);
        $reqData = $request->all();

        $reqData['status'] = $request->has('status') ? 1 : 0;
        Region::create($reqData);
        return redirect()->route('region.index')->withStatus(__('region is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function edit(Region $region)
    {
        //
        return view('admin.region.edit', compact('region'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Region $region)
    {
        //
        $request->validate([
            'name' => 'bail|required|max:255',

        ]);
        $reqData = $request->all();

        $reqData['status'] = $request->has('status') ? 1 : 0;
        $region->update($reqData);

        return redirect()->route('region.index')->withStatus(__('region is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        //
        $region->delete();

        return back()->withStatus(__('region is deleted successfully.'));
    }
    public function apiIndex()
    {
        $data = Region::where('status', 1)->orderBy('name', 'asc')->get();

        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function apiIndexAll()
    {
        $data = Region::orderBy('name', 'asc')->get();

        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
}
