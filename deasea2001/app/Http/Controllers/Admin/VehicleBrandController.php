<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use Illuminate\Http\Request;

class VehicleBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vehicleBrand = VehicleBrand::all();

        return view('admin.vehicleBrand.index', compact('vehicleBrand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.vehicleBrand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required|unique:vehicle_brand|max:255',
            'icon' => 'bail|required|image',
        ]);
        $reqData = $request->all();
        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }
        $reqData['status'] = $request->has('status') ? 1 : 0;
        VehicleBrand::create($reqData);
        return redirect()->route('vehicle-brand.index')->withStatus(__('vehicleBrand is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VehicleBrand  $vehicleBrand
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleBrand $vehicleBrand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VehicleBrand  $vehicleBrand
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleBrand $vehicleBrand)
    {
        //
        return view('admin.vehicleBrand.edit', compact('vehicleBrand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VehicleBrand  $vehicleBrand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleBrand $vehicleBrand)
    {
        //
        $request->validate([
            'name' => 'bail|required|max:255',
            'icon' => 'bail|sometimes|required|image',
        ]);
        $reqData = $request->all();
        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }
        $reqData['status'] = $request->has('status') ? 1 : 0;
        $vehicleBrand->update($reqData);

        return redirect()->route('vehicle-brand.index')->withStatus(__('vehicleBrand is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VehicleBrand  $vehicleBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleBrand $vehicleBrand)
    {
        //
        $vehicleBrand->delete();

        return back()->withStatus(__('Vehicle Brand is deleted successfully.'));
    }

    public function apiIndex()
    {
        $data = VehicleBrand::where('status', 1)->orderBy('name', 'asc')->get(['id', 'name', 'icon']);

        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function vehicleModel($id)
    {
        $data = VehicleModel::where('brand_id',$id)->orderBy('name', 'asc')->get(['id', 'name']);

        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
}
