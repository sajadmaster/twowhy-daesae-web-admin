<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AppUsers;
use App\Models\PointPackage;
use App\Models\ShopOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PointPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pointPackage = PointPackage::all();

        return view('admin.pointPackage.index', compact('pointPackage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pointPackage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required|unique:point_package|max:50',
            'detail' => 'bail|required|max:255',
            'price' => 'bail|required|numeric',
            'point' => 'bail|required|numeric',

        ]);
        $reqData = $request->all();

        $reqData['status'] = $request->has('status') ? 1 : 0;
        PointPackage::create($reqData);
        return redirect()->route('point-package.index')->withStatus(__('Point Package is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PointPackage  $pointPackage
     * @return \Illuminate\Http\Response
     */
    public function show(PointPackage $pointPackage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PointPackage  $pointPackage
     * @return \Illuminate\Http\Response
     */
    public function edit(PointPackage $pointPackage)
    {
        //
        return view('admin.pointPackage.edit', compact('pointPackage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PointPackage  $pointPackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PointPackage $pointPackage)
    {
        //
        $request->validate([
            'name' => 'bail|required|max:50',
            'detail' => 'bail|required|max:255',
            'price' => 'bail|required|numeric',
            'point' => 'bail|required|numeric',

        ]);
        $reqData = $request->all();

        $reqData['status'] = $request->has('status') ? 1 : 0;
        $pointPackage->update($reqData);

        return redirect()->route('point-package.index')->withStatus(__('Point Package is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PointPackage  $pointPackage
     * @return \Illuminate\Http\Response
     */
    public function destroy(PointPackage $pointPackage)
    {
        $pointPackage->delete();

        return back()->withStatus(__('Point Package is deleted successfully.'));
        //
    }
    public function apiIndex()
    {
        $data = PointPackage::where('status', 1)->orderBy('point', 'asc')->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function buyNewPoint(Request $request)
    {
        $user = AppUsers::find(Auth::id());
        $user->deposit($request->point, ['payment_token' => $request->payment_token, "price" => $request->price]);
        return response()->json(['msg' => "Purchases completed successfully.", 'data' => null, 'success' => true], 200);
    }
    public function point()
    {
        $user = AppUsers::find(Auth::id());
        return response()->json(['msg' => null, 'data' =>  $user->balance, 'success' => true], 200);
    }
    public function pointOwner()
    {
        $user = ShopOwner::find(Auth::id());
        return response()->json(['msg' => null, 'data' =>  $user->balance, 'success' => true], 200);
    }
}
