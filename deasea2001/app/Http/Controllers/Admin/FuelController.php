<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Fuel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FuelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $fuel = Fuel::all();

        return view('admin.fuel.index', compact('fuel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.fuel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required|unique:fuel|max:255',

        ]);
        $reqData = $request->all();

        $reqData['status'] = $request->has('status') ? 1 : 0;
        Fuel::create($reqData);
        return redirect()->route('fuel.index')->withStatus(__('fuel is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fuel  $fuel
     * @return \Illuminate\Http\Response
     */
    public function show(Fuel $fuel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fuel  $fuel
     * @return \Illuminate\Http\Response
     */
    public function edit(Fuel $fuel)
    {
        //
        return view('admin.fuel.edit', compact('fuel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Fuel  $fuel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fuel $fuel)
    {
        //
        $request->validate([
            'name' => 'bail|required|max:255',

        ]);
        $reqData = $request->all();

        $reqData['status'] = $request->has('status') ? 1 : 0;
        $fuel->update($reqData);

        return redirect()->route('fuel.index')->withStatus(__('fuel is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fuel  $fuel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fuel $fuel)
    {
        //
        $fuel->delete();

        return back()->withStatus(__('fuel is deleted successfully.'));
    }
    public function apiIndex()
    {
        $data = Fuel::where('status', 1)->orderBy('name', 'asc')->get(['id', 'name']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
}
