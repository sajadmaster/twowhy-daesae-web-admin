<?php

namespace App\Http\Controllers;

use App\Models\AdminSetting;
use App\Models\AppUsers;
use App\Models\BookingReview;
use App\Models\BookingReviewImage;
use App\Models\ProductReview;
use App\Models\ProductReviewImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductReviewController extends Controller
{
    //
    public function store(Request $request)
    {
        $request->validate([
            'order_id' => 'bail|required',
            'shop_id' => 'bail|required',
            'star' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();
        $data = ProductReview::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                ProductReviewImage::create([
                    "review_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }

        $set  = AdminSetting::first();
        if ($set->free_point == 1) {
            $user = AppUsers::find(Auth::id());
            $user->deposit($set->review_point, ['payment_token' => "Using Give Review"]);
        }

        return response()->json(['msg' => "Review submitted successfully.", 'data' => $data, 'success' => true], 201);
    }
    public function bookingReview(Request $request)
    {
        $request->validate([
            'order_id' => 'bail|required',
            'shop_id' => 'bail|required',
            'star' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();
        $data = BookingReview::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                BookingReviewImage::create([
                    "review_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }

        $set  = AdminSetting::first();
        if ($set->free_point == 1) {
            $user = AppUsers::find(Auth::id());
            $user->deposit($set->review_point, ['payment_token' => "Using Give Review"]);
        }
        return response()->json(['msg' => "Review submitted successfully.", 'data' => $data, 'success' => true], 201);
    }
}
