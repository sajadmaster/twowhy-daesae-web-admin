<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawalApplication extends Model
{
    use HasFactory;
    protected $fillable = [
        'owner_id', 'req_point', 'transaction_id', 'status', 'remark', 
    ];
    protected $table = 'withdrawal_application';
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('latest', function (Builder $builder) {
            $builder->orderby('created_at', 'desc');
        });
    }
    public function Owner()
    {
        return $this->belongsTo('App\Models\ShopOwner', 'owner_id', 'id');
    }
}
