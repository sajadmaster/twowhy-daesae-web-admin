<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingChild extends Model
{
    use HasFactory;
    protected $dates = [
        'start_time',

    ];
    protected $fillable = [
        'master_id', 'start_time', 'service_type', 'price', 'status'
    ];
    public $table = 'booking_child';
    protected $hidden = [
        'updated_at',

        'created_at',
    ];
    public function Booking()

    {
        return $this->belongsTo('App\Models\BookingMaster', 'master_id', 'id');
    }
    // protected $with = ['service'];
    // public function Service()
    // {
    //     return $this->belongsTo('App\Models\OwnerService', 'service_id', 'id')->select(['id', 'name']);
    // }
}
