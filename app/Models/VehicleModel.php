<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $hidden = [
        'updated_at',
        'created_at',

    ];
    public $table = 'vehicle_model';
    public function Brand()
    {
        return $this->belongsTo('App\Models\VehicleBrand', 'brand_id', 'id');
    }
}
