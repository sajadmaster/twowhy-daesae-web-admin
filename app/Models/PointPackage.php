<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointPackage extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $table = 'point_package';
    protected $hidden = [
        'updated_at',
        'created_at',
    ];
}
