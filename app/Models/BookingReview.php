<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingReview extends Model
{
    use HasFactory;
    public $table = 'booking_review';

    protected $dates = [
        'created_at',
        'updated_at',

    ];

    protected $fillable = [
        'user_id',
        'shop_id',
        'order_id',
        'star',
        'msg',

    ];
    protected $appends = ['user'];
    public function Images()
    {
        return $this->hasMany('App\Models\BookingReviewImage', 'review_id', 'id');
    }
    public function getUserAttribute()
    {

        return AppUsers::find($this->attributes['user_id'], ['name', 'id', 'image']);
    }
    public function User()
    {
        return $this->belongsTo('App\AppUsers', 'user_id', 'id');
    }
    public function Shop()
    {
        return $this->belongsTo('App\Models\OwnerShop', 'shop_id', 'id');
    }

    public function Employee()
    {
        return $this->belongsTo('App\OwnerShop', 'branch_id', 'id');
    }
}
