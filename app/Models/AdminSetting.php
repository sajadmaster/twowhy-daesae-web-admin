<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminSetting extends Model
{
    use HasFactory;
    protected $fillable = [
        'pp', 'notification', 'currency_symbol', 'currency', 'verification', 'country_code', 'paypal_status', 'razor_status', 'phone_no', 'email', 'address', 'ios_version', 'android_version', 'offline_payment', 'time_slot_length', 'admin_per', 'free_point', 'review_point', 'address_point', 'radius'
    ];
    protected $table = 'admin_setting';
}
