<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerProduct extends Model
{
    use HasFactory;
    protected $fillable = [
        'owner_id', 'name', 'detail', 'cat_id', 'sub_cat_id', 'status', 'price','enable_discount','discount_price'
    ];
    protected $table = 'owner_product';
    public function Images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id', 'id');
    }
  
    public function Category()
    {
        return $this->belongsTo('App\Models\Category', 'cat_id', 'id');
    }
    public function SubCategory()
    {
        return $this->belongsTo('App\Models\SubCategory', 'sub_cat_id', 'id');
    }
}
