<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocationImageData extends Model
{
    use HasFactory;
    public $table = 'my_vehicle_location_detail';

    protected $fillable = [
        'user_id',
        'vehicle_img',

    ];
    protected $appends = ['user'];
    public function Images()
    {
        return $this->hasMany('App\Models\LocationImage', 'user_id', 'id');
    }
    public function getUserAttribute()
    {
        return AppUsers::find($this->attributes['user_id'], ['name', 'id', 'vehicle_img']);
    }
    public function User()
    {
        return $this->belongsTo('App\AppUsers', 'user_id', 'id');
    }
    public function Shop()
    {
        return $this->belongsTo('App\Models\OwnerShop', 'shop_id', 'id');
    }
    public function Employee()
    {
        return $this->belongsTo('App\OwnerShop', 'branch_id', 'id');
    }
}