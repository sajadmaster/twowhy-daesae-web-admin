<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class announce extends Model
{
    use HasFactory;
    public $table = 'announce';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title', 'detail', 'icon', 'tag', 'views', 'status'
    ];

    protected $appends = ['imageUri'];
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['icon'])) {
            return url('upload/') . '/' . $this->attributes['icon'];
        }
    }
    public function setTagAttribute($value)
    {
        $this->attributes['tag'] = implode(',', $value);
    }

    public function getTagAttribute($value)
    {
        return explode(',', $value);
    }
}
