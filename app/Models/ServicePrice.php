<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicePrice extends Model
{
    use HasFactory;
    protected $fillable = [
        'service_id', 'size_id', 'in_price', 'out_price'
    ];
    protected $table = 'service_price';


    function size()
    {
        return $this->belongsTo('App\Models\VehicleSize', 'size_id', 'id');
         
    }
}
