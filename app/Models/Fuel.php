<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fuel extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $table = 'fuel';
    protected $hidden = [
        'updated_at',
        'created_at',
    ];
}
