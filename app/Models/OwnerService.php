<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OwnerService extends Model
{
    use HasFactory;
    protected $fillable = [
        'owner_id', 'name', 'detail', 'cat_id', 'sub_cat_id', 'sell_at', 'discount', 'status'
    ];
    protected $table = 'owner_service';
    public function Images()
    {
        return $this->hasMany('App\Models\ServiceImage', 'service_id', 'id');
    }
    public function Prices()
    {
        return $this->hasMany('App\Models\ServicePrice', 'service_id', 'id');
    }
    public function Category()
    {
        return $this->belongsTo('App\Models\Category', 'cat_id', 'id');
    }
    public function SubCategory()
    {
        return $this->belongsTo('App\Models\SubCategory', 'sub_cat_id', 'id');
    }
}
