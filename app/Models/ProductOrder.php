<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'booking_no', 'shop_id', 'owner_id', 'item_id', 'type', 'admin_per', 'amount', 'payment_status', 'status', 'discount', 'qty','employee_id'
    ];
    protected $table = 'product_order';
    public function Shop()
    {
        return $this->belongsTo('App\Models\OwnerShop', 'shop_id', 'id');
    }
    public function Item()
    {
        return $this->belongsTo('App\Models\OwnerProduct', 'item_id', 'id');
    }
    public function User()
    {
        return $this->belongsTo('App\Models\AppUsers', 'user_id', 'id');
    }
    public function Review()
    {
        return $this->hasOne('App\Models\ProductReview', 'order_id', 'id');
    }
    public function Employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('latest', function (Builder $builder) {
            $builder->orderby('created_at', 'desc');
        });
    }
}
