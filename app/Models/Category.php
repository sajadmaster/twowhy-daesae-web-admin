<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public $table = 'categories';

    protected $dates = [
        'created_at',
        'updated_at',
      
    ];

    protected $fillable = [
        'name', 'icon', 'status'
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        
    ];
    protected $appends = ['imageUri'];
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['icon'])) {

            return url('upload/') . '/' . $this->attributes['icon'];
        }
    }
  
}
