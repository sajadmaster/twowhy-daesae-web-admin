<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserVehicle extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $table = 'user_vehicle';
    protected $hidden = [
        'updated_at',
        'created_at',

    ];
    public function Model()
    {
        return $this->belongsTo('App\Models\VehicleModel', 'model_id', 'id');
    }
    public function Brand()
    {
        return $this->belongsTo('App\Models\VehicleBrand', 'brand_id', 'id');
    }
    public function Fuel()
    {
        return $this->belongsTo('App\Models\Fuel', 'fuel_id', 'id');
    }
    public function User()
    {
        return $this->belongsTo('App\Models\AppUsers', 'user_id', 'id');
    }
}
