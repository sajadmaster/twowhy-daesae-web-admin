<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocationImage extends Model
{
    use HasFactory;
    protected $fillable = [
        'vehicle_img',
    ];
    public $timestamps = false;
    protected $table = 'my_vehicle_location_detail';
    
    protected $appends = ['imageUri'];
    
    // public function Images()
    // {
    //     return $this->hasMany('App\Models\LocationImage', 'vehicle_img', 'id');
    // }
    
    public function getImageUriAttribute()
    {
        if (isset($this->attributes['vehicle_img'])) {

            return url('upload/') . '/' . $this->attributes['vehicle_img'];
        }
    }
}
