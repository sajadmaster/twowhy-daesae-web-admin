<?php

namespace App\Http\Controllers;

use App\Models\AdminSetting;
use App\Models\AppUsers;
use App\Models\BookingChild;
use App\Models\BookingMaster;
use App\Models\OwnerShop;
use App\Models\ShopOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class BookingMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function index()
    {

        $data =   BookingMaster::with(['review', 'shop:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item'])->where('user_id', Auth::id())->get();

        foreach ($data as  $value) {
            if ($value->status == 1 ||  $value->status == 4) {

                $c = BookingChild::where('master_id', $value->id)->groupBy('status')->get();
                if (count($c) == 1) {
                    $value->status  = $c[0]->status;
                } else {
                    // change to parisal
                    $value->status  = 4;
                }
                $value->save();
            }
        }
        //
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function futureBooking()
    {
        $ids =   BookingMaster::where('user_id', Auth::id())->get()->pluck('id');
        $data = BookingChild::whereIn('master_id', $ids)->where([['start_time', '>=', Carbon::today()], ['status', '<', 3]])->get();
        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['start_time_date'] = $data[$i]['start_time']->format('Y-m-d');
        }
        // foreach ($data as $key => $value) {
        //     dd($value->start_time->format('Y-m-d'));
        //     $data[$key]['start_time'] = $value->start_time->format('Y-m-d');
        // }
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function futureBookingDate(Request $request)
    {
        $ids =   BookingMaster::where('user_id', Auth::id())->get()->pluck('id');
        $data = BookingChild::whereIn('master_id', $ids)->where([['status', '<', 3]])->whereDate('start_time', $request->date)->get();
        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['start_time_date'] = $data[$i]['start_time']->format('Y-m-d');
        }
        // foreach ($data as $key => $value) {
        //     dd($value->start_time->format('Y-m-d'));
        //     $data[$key]['start_time'] = $value->start_time->format('Y-m-d');
        // }
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function scheduleBooking()
    {
        $data =   BookingMaster::with(['review', 'shop:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item.service'])->where('user_id', Auth::id())->whereDate('start_time', '>=', Carbon::today())->get();
        //
        return response()->json(['msg' => 'Booking added', 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'address' => 'bail|required',
            'vehicle_id' => 'bail|required',
            // 'start_time' => 'bail|required|date_format:Y-m-d H:i:s|after:now',
            'amount' => 'bail|required',
            'discount' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['booking_id'] = substr(uniqid('bhk-'), 0, 9);
        $reqData['user_id'] = Auth::id();
        $reqData['admin_per'] = AdminSetting::first()->admin_per;

        $data = BookingMaster::create($reqData);
        $child = json_decode($request->child, true);
        foreach ($child as  $value) {

            BookingChild::create([
                'master_id' => $data->id,
                'start_time' => $value['start_time'] . " " . $request->time,
                'price' => $value['price'],
                'service_type' => $value['service_type'],
            ]);
        }
        $user = AppUsers::find(Auth::id());

        $user->withdraw($reqData['amount'], ['booking' => $data['id'], 'booking_type' => "booking"]);

        $ids['user_id'] = $data['user_id'];
        $ids['owner_id'] = $data['owner_id'];
        $ids['bid'] = $data['id'];
        $ids['employee_id'] = $data['employee_id'];
        $rid = $data->region;
        $oids = ShopOwner::where([['is_daesea', 1], ['status', 1]])->get('id')->pluck('id');
        $ownerIds = OwnerShop::whereIn('owner_id', $oids)->where(function (Builder $query) use ($rid) {
            return $query->whereRaw('FIND_IN_SET(?, regions)', [$rid]);
        })->get()->pluck('owner_id');
        foreach ($ownerIds as  $value) {

            $ids['owner_id'] = $value;

            try {
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 1);
            } catch (\Throwable $th) {
                //  throw $th;
            }
        }

        // $shop = OwnerShop::whereIn('owner_id', $oids)->where(function (Builder $query) use ($rid) {
        //     return $query->whereRaw('FIND_IN_SET(?, regions)', [$rid]);
        // })->where(function (Builder $query) use ($empid) {
        //     return $query->whereRaw('FIND_IN_SET(?, employee)', [$empid]);
        // })->first();


        return response()->json(['msg' => 'Booking added', 'data' => null, 'success' => true], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BookingMaster  $bookingMaster
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BookingMaster  $bookingMaster
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BookingMaster  $bookingMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookingMaster  $bookingMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function ownerIndex()
    {

        if (Auth::getDefaultDriver() == "manager") {
            $ids = (new AppHelper)->managerShop();

            $data =   BookingMaster::with(['review', 'shop:id,name,image', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item', 'employee:id,name'])->where('owner_id', Auth::user()->owner_id)->whereIn('shop_id',  $ids)->get();
        } else {
            $data =   BookingMaster::with(['review', 'shop:id,name,image', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item', 'employee:id,name'])->where('owner_id', Auth::id())->get();
        }
        $s = "";
        foreach ($data as  $value) {
            if ($value->status == 1 ||   $value->status == 4){
                $c = BookingChild::where('master_id', $value->id)->groupBy('status')->get();
            if (count($c) == 1) {
                // $s = $c;
                $value->status = $c[0]->status;
            } else {
                // change to parisal
                $value->status  = 4;
            }
            $value->save();
            }
        }
        // return $s;
        //
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    public function BookingListOwner()
    {
        $data ='';
          
        $data =   BookingMaster::with(['review', 'shop:id,name,image', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item', 'employee:id,name'])->where('owner_id', Auth::user()->owner_id)->get();
        
        
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    
    public function childUpdate($id, Request $request)
    {
        $sdata = BookingChild::find($id);
        $data = BookingMaster::find($sdata->master_id);
        $ids['user_id'] = $data['user_id'];
        $ids['owner_id'] = $data['owner_id'];
        $ids['bid'] = $data['id'];
        $ids['employee_id'] = $data['employee_id'];
        if (isset($request->status) && $request->status == 4) {
            $user = AppUsers::find($data['user_id']);
            $user->deposit($sdata['price'], ['booking' => $sdata['id'], 'booking_type' => "booking", "refund" => "yes"]);

            try {
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 5);
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 6);
            } catch (\Throwable $th) {
                //  throw $th;
            }
        }
        if (isset($request->status) && $request->status == 2) {

            try {
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 7);
            } catch (\Throwable $th) {
                //  throw $th;
            }
        }
        if (isset($request->status) && $request->status == 3) {
            $user = AppUsers::find($data['user_id']);
            $owner = ShopOwner::find($data['owner_id']);
            $amount = $sdata['price'];
            if ($data['admin_per'] > 0) {
                $amount = ($sdata['price'] *  $data['admin_per']) / 100;
                $amount =  $sdata['price'] - $amount;
            }
            $user->transfer($owner, $amount, ["booking" => $sdata['id'], "type" => "booking"]);
            $user->deposit($amount, ['payment_token' => "Un-known"]);
            try {

                $res = (new Admin\StaticNotiController)->baseNotification($ids, 4);
            } catch (\Throwable $th) {
                //   throw $th;
            }
        }
        $sdata->update($request->all());
        $data->update($request->all());
        return response()->json(['msg' => "Data Updated", 'data' => $data, 'success' => true], 200);
    }
    public function updateBooking($id, Request $request)
    {
        $data = BookingMaster::find($id);
        $data->update($request->all());
        
        $ids['user_id'] = $data['user_id'];
        $ids['owner_id'] = $data['owner_id'];
        $ids['bid'] = $data['id'];
        $ids['employee_id'] = $data['employee_id'];
        BookingChild::where('master_id', $data->id)->update(['status' => 1]);
        
        if (isset($request->status) && $request->status == 1) {
            try {
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 2);
            } catch (\Throwable $th) {
                //  throw $th;
            }
        }

        return response()->json(['msg' => '예약이 업데이트되었습니다', 'data' => null, 'success' => true], 200);
    }
    public function waitingBookingManger()
    {
        $id = 1;


        if (Auth::getDefaultDriver() == "manager") {
        $ids = (new AppHelper)->managerShop();

        $master = array();
        $oids = ShopOwner::where([['is_daesea', 1], ['status', 1]])->get('id')->pluck('id');
        $booking = BookingMaster::where('shop_id', 0)->groupBy('region')->get();
        foreach ($booking as  $value) {
            $rid = $value->region;
            $shop = OwnerShop::whereIn('owner_id', $oids)->whereIn('id', $ids)->where(function (Builder $query) use ($rid) {
                return $query->whereRaw('FIND_IN_SET(?, regions)', [$rid]);
            })->first(['id', 'name', 'address', 'phone_no', 'image', 'start_time', 'end_time', 'owner_id']);
            if ($shop) {
                $bd = BookingMaster::with(['review', 'item', 'shop:id,name,image', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'employee:id,name'])->where([['shop_id', 0], ['region', $rid]])->get();
                foreach ($bd as  $b) {
                    # code...
                    $b['shop_id_temp'] = $shop->id;
                }
                // array_push($master, $bd);
                $master = array_merge($master, $bd->toArray());
            }
        }
        
        }
        else {
            
        $master = array();
        $oids = ShopOwner::where([['is_daesea', 1], ['status', 1], ['id', Auth::id()]])->get('id')->pluck('id');
        $booking = BookingMaster::where('shop_id', 0)->groupBy('region')->get();
        foreach ($booking as  $value) {
            $rid = $value->region;
            $shop = OwnerShop::whereIn('owner_id', $oids)->where(function (Builder $query) use ($rid) {
                return $query->whereRaw('FIND_IN_SET(?, regions)', [$rid]);
            })->first(['id', 'name', 'address', 'phone_no', 'image', 'start_time', 'end_time', 'owner_id']);
            if ($shop) {
                $bd = BookingMaster::with(['review', 'item', 'shop:id,name,image', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'employee:id,name'])->where([['shop_id', 0], ['region', $rid]])->get();
                foreach ($bd as  $b) {
                    # code...
                    $b['shop_id_temp'] = $shop->id;
                }
                // array_push($master, $bd);
                $master = array_merge($master, $bd->toArray());
            }
        }
        }
        return response()->json(['msg' => null, 'data' => $master, 'success' => true], 200);
    }
    
    public function waitingBookingEmployee()
    {
        $id = 1;

        // $ids = (new AppHelper)->managerShop();

        $master = array();
        $empid = Auth::id();
        $lat = Auth::user()->lat;
        $lng =    Auth::user()->lng;
        $oids = ShopOwner::where([['is_daesea', 1], ['status', 1]])->get('id')->pluck('id');
        // dd($oids);
        $booking = BookingMaster::where('shop_id', 0)->groupBy('region')->get();
        // dd($booking);
        foreach ($booking as  $value) {
            $rid = $value->region;
            $shop = OwnerShop::whereIn('owner_id', $oids)->where(function (Builder $query) use ($rid) {
                return $query->whereRaw('FIND_IN_SET(?, regions)', [$rid]);
            })->where(function (Builder $query) use ($empid) {
                return $query->whereRaw('FIND_IN_SET(?, employee)', [$empid]);
            })->first();
            if ($shop) {
                // dd($shop);

                //employee 
                // $data = FuelProvider::where([['status', 1], ['verified', 1], ['is_online', 1]])->GetByDistance($request->lat, $request->lang, $request->radius)->get(['id', 'name', 'lat', 'lang', 'image']);

                $bd = BookingMaster::with(['review', 'shop:id,name,image', 'user:id,name,image', 'item', 'address:id,address_name,type', 'Vehicle:id,name,number', 'employee:id,name'])->where([['shop_id', 0], ['region', $rid]])->get();
                foreach ($bd as  $b) {
                    # code...
                    $b['shop_id_temp'] = $shop->id;
                }
                $master = array_merge($master, $bd->toArray());
            }
        }
        return response()->json(['msg' => null, 'data' => $master, 'success' => true], 200);
    }
}
