<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\announce;
use Illuminate\Http\Request;

class announceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $announces = announce::get();

        return view('admin.announce.index', compact('announces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.announce.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'bail|required|max:255',
            'detail' => 'bail|required',
            'tag' => 'bail|required',
        ]);
        $reqData = $request->all();

        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }

        $reqData['status'] = $request->has('status') ? 1 : 0;
        announce::create($reqData);
        return redirect()->route('announce.index')->withStatus(__('announce is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\announce  $announce
     * @return \Illuminate\Http\Response
     */
    public function show(announce $announce)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\announce  $announce
     * @return \Illuminate\Http\Response
     */
    public function edit(announce $announce)
    {
        //
        return view('admin.announce.edit', compact('announce'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\announce  $announce
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, announce $announce)
    {
        //
        $request->validate([
            'title' => 'bail|required|max:255',
            'detail' => 'bail|required',
            'tag' => 'bail|required',
        ]);
        $reqData = $request->all();

        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }

        $reqData['status'] = $request->has('status') ? 1 : 0;


        $announce->update($reqData);

        return redirect()->route('announce.index')->withStatus(__('announce is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\announce  $announce
     * @return \Illuminate\Http\Response
     */
    public function destroy(announce $announce)
    {
        //
        $announce->delete();

        return back()->withStatus(__('announce is deleted successfully.'));
    }

    public function apiIndex()
    {
        $announce = announce::where('status', 1)->latest()->get();
        return response()->json(['msg' => null, 'data' => $announce, 'success' => true], 200);
    }
    public function singleannounce($id)
    {
        $announce = announce::find($id);
        $announce->increment('views', 1);
        return response()->json(['msg' => null, 'data' => $announce, 'success' => true], 200);
    }
}
