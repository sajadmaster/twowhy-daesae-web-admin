<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;

use App\Models\Notice;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    public function index()
    {
        //
        $notices = Notice::get();

        return view('admin.notice.index', compact('notices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'bail|required|max:255',
            'detail' => 'bail|required',
        ]);
        $reqData = $request->all();

        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }

        $reqData['status'] = $request->has('status') ? 1 : 0;
        $reqData['for_who'] = $request->has('for_who') ? 1 : 0;
        Notice::create($reqData);
        return redirect()->route('notice.index')->withStatus(__('Notice is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function show(Notice $notice)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function edit(Notice $notice)
    {
        //
        return view('admin.notice.edit', compact('notice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notice $notice)
    {
        //
        $request->validate([
            'title' => 'bail|required|max:255',
            'detail' => 'bail|required',
        ]);
        $reqData = $request->all();

        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }

        $reqData['status'] = $request->has('status') ? 1 : 0;

$reqData['for_who'] = $request->has('for_who') ? 1 : 0;
        $notice->update($reqData);

        return redirect()->route('notice.index')->withStatus(__('Notice is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Notice  $notice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notice $notice)
    {
        //
        $notice->delete();

        return back()->withStatus(__('notice is deleted successfully.'));
    }

    public function apiIndex()
    {
        $notice = Notice::where([['status', 1],['for_who',0]])->latest()->get();
        return response()->json(['msg' => null, 'data' => $notice, 'success' => true], 200);
    }
      public function apiIndexManager()
    {
        $notice = Notice::where([['status', 1],['for_who',1]])->latest()->get();
        return response()->json(['msg' => null, 'data' => $notice, 'success' => true], 200);
    }
    public function singleNotice($id)
    {
        $notice = Notice::find($id);
        
        return response()->json(['msg' => null, 'data' => $notice, 'success' => true], 200);
    }
}
