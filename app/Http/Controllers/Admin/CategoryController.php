<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required|unique:categories|max:255',
            'icon' => 'bail|required|image',
        ]);
        $reqData = $request->all();
        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }

        $reqData['status'] = $request->has('status') ? 1 : 0;
        Category::create($reqData);
        return redirect()->route('categories.index')->withStatus(__('Category is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $request->validate([
            'name' => 'bail|required|max:255',
            'icon' => 'bail|sometimes|required|image',
        ]);
        $reqData = $request->all();
        if ($request->icon && $request->icon != "undefined") {
            $reqData['icon'] = (new AppHelper)->saveImage($request);
        }

        $reqData['status'] = $request->has('status') ? 1 : 0;
        $category->update($reqData);

        return redirect()->route('categories.index')->withStatus(__('Category is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        $category->delete();

        return back()->withStatus(__('Category is deleted successfully.'));
    }
    public function apiIndex()
    {
        $categories = Category::where('status', 1)->orderBy('name', 'asc')->get();

        return response()->json(['msg' => null, 'data' => $categories, 'success' => true], 200);
    }
    public function subCategory($id)
    {
        $data = SubCategory::where([['status', 1], ['cat_id', $id]])->orderBy('name', 'asc')->get();

        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function multiSubCategory(Request $request)
    {
        $data = SubCategory::whereIn('cat_id', explode(',', $request->ids))->where([['status', 1]])->orderBy('name', 'asc')->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
}
