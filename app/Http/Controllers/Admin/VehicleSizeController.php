<?php


namespace App\Http\Controllers\Admin; 
use App\Http\Controllers\Controller;
use App\Models\VehicleSize;
use Illuminate\Http\Request;

class VehicleSizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $size = VehicleSize ::all();

        return view('admin.vehicleSize.index', compact('size'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vehicleSize.create');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required|unique:categories|max:255',
            
        ]);
        $reqData = $request->all();
        

        $reqData['status'] = $request->has('status') ? 1 : 0;
        VehicleSize ::create($reqData);
        return redirect()->route('vehicle-size.index')->withStatus(__('vehicle Size is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VehicleSize  $vehicleSize
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleSize $vehicleSize)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VehicleSize  $vehicleSize
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleSize $vehicleSize)
    {
        //
        return view('admin.vehicleSize.edit', compact('vehicleSize'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VehicleSize  $vehicleSize
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleSize $vehicleSize)
    {
        //
        $request->validate([
            'name' => 'bail|required|max:255',
           
        ]);
        $reqData = $request->all();
       

        $reqData['status'] = $request->has('status') ? 1 : 0;
        $vehicleSize->update($reqData);

        return redirect()->route('vehicle-size.index')->withStatus(__('vehicle Size is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VehicleSize  $vehicleSize
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleSize $vehicleSize)
    {
        //
        $vehicleSize->delete();

        return back()->withStatus(__('vehicle Size is deleted successfully.'));
    }
    public function apiIndex()
    {
        $data = VehicleSize::where('status', 1)->orderBy('name', 'asc')->get();

        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
}
