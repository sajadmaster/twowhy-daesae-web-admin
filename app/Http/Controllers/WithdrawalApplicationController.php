<?php

namespace App\Http\Controllers;

use App\Models\ShopOwner;
use App\Models\WithdrawalApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WithdrawalApplicationController extends Controller
{
    //
    public function reqNew(Request $request)
    {
        $request->validate([
            'req_point' => 'bail|required'
        ]);
        $owner = ShopOwner::find(Auth::id());
        $b = $owner->balance;
        $reqData = $request->all();
        $reqData['owner_id'] = Auth::id();
        $p = WithdrawalApplication::where([['owner_id', Auth::id()], ['status', '<', 2]])->get()->count();
        if ($p == 0) {

            if ($b >= $request->req_point) {
                WithdrawalApplication::create($reqData);
                return response()->json(['msg' => "Your request submitted successfully it tack 24hour.", 'data' => null, 'success' => true], 200);
            } else {
                return response()->json(['msg' => "You Don`t have enough point to withdraw", 'data' => null, 'success' => false], 200);
            }
        }
        return response()->json(['msg' => "You have already submitted request please wait to resolve past request", 'data' => null, 'success' => false], 200);
    }
    public function revokeRew($id)
    {
        $da = WithdrawalApplication::find($id);

        return response()->json(['msg' => "Your request revoke successfully.", 'data' => null, 'success' => true], 200);
    }
    public function index()
    {
        $data =  WithdrawalApplication::with('owner:id,name')->get();

        return view('admin.withdrawal.index', compact('data'));
    }
    public function OwnerIndex()
    {
        $data =  WithdrawalApplication::where('owner_id', Auth::id())->get();
        return response()->json(['msg' => null, 'data' =>   $data, 'success' => true], 200);
    }
    public function show($id)
    {
        $data =  WithdrawalApplication::find($id);
        $data->load('owner');
        return view('admin.withdrawal.show', compact('data'));
    }
    public function update($id, Request $request)
    {
        $data =  WithdrawalApplication::find($id);
        $data->update($request->all());
        if ($request->status == 2) {
            $owner = ShopOwner::find($data->owner_id);
            $owner->withdraw($data->req_point);
        }
        return redirect()->route('withdrawal.index')->withStatus(__('Status is updated successfully.'));
    }
}
