<?php

namespace App\Http\Controllers;

use App\Models\AppUsers;
use App\Models\ShopOwner;
use App\Models\WithdrawalApplication;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    //
    public function userReport(Request $request)
    {
        if ($request->has('start_date') && $request->has('end_date')) {
            $data = AppUsers::whereBetween('created_at', [$request->start_date, $request->end_date])->get();
        } else {

            $data = AppUsers::get();
        }
        return view('admin.report.user', compact('data'));
        # code...
    }
    public function ownerReport(Request $request)
    {
        if ($request->has('start_date') && $request->has('end_date')) {
            $data = ShopOwner::whereBetween('created_at', [$request->start_date, $request->end_date])->get();
        } else {

            $data = ShopOwner::get();
        }
        return view('admin.report.owner', compact('data'));
        # code...
    }
    public function withdrawalReport(Request $request)
    {

        if ($request->has('start_date') && $request->has('end_date')) {
            $data = WithdrawalApplication::with('owner:id,name')->whereBetween('created_at', [$request->start_date, $request->end_date])->get();
        } else {

            $data =  WithdrawalApplication::with('owner:id,name')->get();
        }
        return view('admin.report.withdrawal', compact('data'));
        # code...
    }
}
