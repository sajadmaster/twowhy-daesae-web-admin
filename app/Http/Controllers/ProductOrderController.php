<?php

namespace App\Http\Controllers;

use App\Models\AdminSetting;
use App\Models\AppUsers;
use App\Models\ProductOrder;
use App\Models\ShopOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductOrderController extends Controller
{
    //
    public function store(Request $request)
    {
        $request->validate([
            'shop_id' => 'bail|required',
            'owner_id' => 'bail|required',
            'item_id' => 'bail|required',
            'type' => 'bail|required',
            'amount' => 'bail|required',
            'payment_status' => 'bail|required',
            'discount' => 'bail|required',
            'qty' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();

        $reqData['booking_no'] = substr(uniqid('pro-'), 0, 9);
        $reqData['admin_per'] = AdminSetting::first()->admin_per;

        $data = ProductOrder::create($reqData);
        // 
        $user = AppUsers::find(Auth::id());

        $user->withdraw($reqData['amount'], ['booking' => $data['id'], 'booking_type' => "product"]);
        //  

        $ids['user_id'] = $data['user_id'];
        $ids['owner_id'] = $data['owner_id'];
        $ids['bid'] = $data['id'];
        $ids['booking_type'] = "Product";
        try {
            $res = (new Admin\StaticNotiController)->baseNotification($ids, 1);
        } catch (\Throwable $th) {
            //  throw $th;
        }

        return response()->json(['msg' => "Product Purchased Successfully", 'data' => null, 'success' => true], 201);
    }
    
 
    public function orderList()
    {

        $data =    ProductOrder::with(['shop:id,name,image', 'item:id,name', 'review'])->where('user_id', Auth::id())->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function orderOwnerList()
    {

        if (Auth::getDefaultDriver() == "manager") {
            $ids = (new AppHelper)->managerShop();
            $data = ProductOrder::with(['shop:id,name', 'user:id,name,image,phone_no', 'item:id,name', 'review', 'employee:id,name'])->where('owner_id', Auth::user()->owner_id)->whereIn('shop_id',  $ids)->get();
        } else {
            $data = ProductOrder::with(['shop:id,name', 'user:id,name,image,phone_no', 'item:id,name', 'review', 'employee:id,name'])->where('owner_id', Auth::id())->get();
        }
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    public function updateOrder($id, Request $request)
    {
        $data = ProductOrder::find($id);
        $ids['user_id'] = $data['user_id'];
        $ids['owner_id'] = $data['owner_id'];
        $ids['bid'] = $data['id'];
        $ids['employee_id'] = $data['employee_id'];
        $ids['booking_type'] = "Product";
      
        if (isset($request->status) && $request->status == 2) {
            $user = AppUsers::find($data['user_id']);
            $user->deposit($data['amount'], ['booking' => $data['id'], 'booking_type' => "booking", "refund" => "yes"]);
            try {
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 5);
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 6);
            } catch (\Throwable $th) {
                //  throw $th;
            }
        }
        if (isset($request->employee_id) && $request->employee_id >  0) {
            try {
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 2);
            } catch (\Throwable $th) {
                //  throw $th;
            }
        }
        if (isset($request->status) && $request->status == 1) {
            $user = AppUsers::find($data['user_id']);
            $owner = ShopOwner::find($data['owner_id']);
            $amount = $data['amount'];
            if ($data['admin_per'] > 0) {
                $amount = ($data['amount'] *  $data['admin_per']) / 100;
                $amount = $data['amount'] - $amount;
            }
            $user->transfer($owner, $amount, ["booking" => $data['id'], "type" => "booking"]);

            try {
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 3);
                $res = (new Admin\StaticNotiController)->baseNotification($ids, 4);
            } catch (\Throwable $th) {
                //  throw $th;
            }
        }
        $data->update($request->all());
        return response()->json(['msg' => 'Booking updated', 'data' => null, 'success' => true], 200);
    }
}
