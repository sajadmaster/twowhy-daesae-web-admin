<?php

namespace App\Http\Controllers;

use App\Models\AdminSetting;
use App\Models\AppUsers;
use App\Models\BookingReview;
use App\Models\BookingReviewImage;
use App\Models\ProductReview;
use App\Models\ProductReviewImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ProductReviewController extends Controller
{
    //
    public function store(Request $request)
    {
        $request->validate([
            'order_id' => 'bail|required',
            'shop_id' => 'bail|required',
            'star' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();
        $data = ProductReview::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                ProductReviewImage::create([
                    "review_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }

        $set  = AdminSetting::first();
        if ($set->free_point == 1) {
            $user = AppUsers::find(Auth::id());
            $user->deposit($set->review_point, ['payment_token' => "Using Give Review"]);
        }

        return response()->json(['msg' => "Review submitted successfully.", 'data' => $data, 'success' => true], 201);
    }
    public function bookingReview(Request $request)
    {
        $request->validate([
            'order_id' => 'bail|required',
            'shop_id' => 'bail|required',
            'star' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();
        $data = BookingReview::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                BookingReviewImage::create([
                    "review_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }

        $set  = AdminSetting::first();
        if ($set->free_point == 1) {
            $user = AppUsers::find(Auth::id());
            $user->deposit($set->review_point, ['payment_token' => "Using Give Review"]);
        }
        return response()->json(['msg' => "Review submitted successfully.", 'data' => $data, 'success' => true], 201);
    }
    
    // work_store
    public function work_store(Request $request)
    {
        // $request->validate([
        //     'order_id' => 'bail|required',
        //     'shop_id' => 'bail|required',
        // ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();
        $data = StartWork::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                StartWorkImage::create([
                    "review_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }

        $set  = AdminSetting::first();
        if ($set->free_point == 1) {
            $user = AppUsers::find(Auth::id());
            $user->deposit($set->review_point, ['payment_token' => "Using Give Review"]);
        }

        return response()->json(['msg' => "Review submitted successfully.", 'data' => $data, 'success' => true], 201);
    }
    
    // work_start 이미지 저장.
    public function workStart(Request $request)
    {
        $request->validate([
            'order_id' => 'bail|required',
            'shop_id' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();
        $data = workStart::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                workStartImage::create([
                    "review_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }

        return response()->json(['msg' => "Review submitted successfully.", 'data' => $data, 'success' => true], 201);
    }
    // childCount 차일드 갯수 리턴.
    public function childCount(Request $request)
    {
        $child_id = $request->input('child_id');
        $master_id = $request->input('master_id');
         
        $data =   DB::table('booking_child')->select('status')->where('master_id', $master_id)->count();
        
        return response()->json(['msg' => '', 'data' => $data, 'success' => true], 200);
    }
    
    // workingStart 일 시작.
    public function workingStart(Request $request)
    {
        $child_id = $request->input('child_id');
        $master_id = $request->input('master_id');
        
        $data =   DB::table('booking_master')->updateOrInsert(
            [
                'id'=>$request->input('master_id')
            ],
            [
                //'id'=>$request->input('master_id'),
                'status' => 6
            ]);
        
        return response()->json(['msg' => '', 'data' => $data, 'success' => true], 200);
    }
    
    // workingEnd 일 끝.
    public function workingEnd(Request $request)
    {
        $child_id = $request->input('child_id');
        $master_id = $request->input('master_id');
        
        $data =   DB::table('booking_master')->updateOrInsert(
            [
                'id'=>$request->input('master_id')
            ],
            [
                //'id'=>$request->input('master_id'),
                'status' => 3
            ]);
        
        return response()->json(['msg' => '', 'data' => $data, 'success' => true], 200);
    }
    
    // workingEnd 일 끝 월간 세차일 경우 차일드가 모두 끝났는지 판단.
    public function workingEnd4(Request $request)
    {
        $child_id = $request->input('child_id');
        $master_id = $request->input('master_id');
        
        $data =   DB::table('booking_child')->where('master_id', $master_id)->pluck('status');
        
        $data = $data->all();
        
        return response()->json(['msg' => '', 'data' => $data, 'success' => true], 200);
    }
    // 파샬이 남아있을 경우
    public function workingNotEnd(Request $request)
    {
        $child_id = $request->input('child_id');
        $master_id = $request->input('master_id');
        
        $data =   DB::table('booking_master')->updateOrInsert(
            [
                'id'=>$request->input('master_id')
            ],
            [
                //'id'=>$request->input('master_id'),
                'status' => 4
            ]);
        
        return response()->json(['msg' => '', 'data' => $data, 'success' => true], 200);
    }
    // 유저 차량 위치 정보 가져오기.
    public function getLocationImageLoad(Request $request) {
        
        $user_id = $request->input('user_id');
        
        $data = DB::table('my_vehicle_location_detail')->where('user_id', $user_id)->get();
        
        
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    public function productReviewUpdate(Request $request,$id)
    {
        $request->validate([
            'order_id' => 'bail|required',
            'shop_id' => 'bail|required',
            'star' => 'bail|required',
        ]);
        $reqData = $request->all();
        // $reqData['user_id'] = Auth::id();
        $data = ProductReview::find($id)->update($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                ProductReviewImage::create([
                    "review_id" => $id,
                    "image" => $i,
                ]);
            }
        }

        $set  = AdminSetting::first();
       
        return response()->json(['msg' => "Review update successfully.", 'data' => $data, 'success' => true], 200);
    }
    public function productReviewImageDelete($id){
        ProductReviewImage::find($id)->delete();
             return response()->json(['msg' => "Review image delete successfull.", 'data' => $data, 'success' => true], 200);
    }
        public function bookingReviewUpdate(Request $request,$id)
    {
        $request->validate([
            'order_id' => 'bail|required',
            'shop_id' => 'bail|required',
            'star' => 'bail|required',
        ]);
        $reqData = $request->all();
        // $reqData['user_id'] = Auth::id();
        $data = BookingReview::find($id)->update($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                BookingReviewImage::create([
                    "review_id" => $id,
                    "image" => $i,
                ]);
            }
        }

        $set  = AdminSetting::first();
       
        return response()->json(['msg' => "Review update successfully.", 'data' => $data, 'success' => true], 200);
    }
    public function bookingReviewImageDelete($id){
        BookingReviewImage::find($id)->delete();
             return response()->json(['msg' => "Review image delete successfully.", 'data' => $data, 'success' => true], 200);
    }
}
