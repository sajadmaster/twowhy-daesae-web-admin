<?php

namespace App\Http\Controllers;

use App\Models\UserVehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\BookingMaster;
use App\Models\BookingChild;


class UserVehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = UserVehicle::with(['model:id,name','brand:id,name,icon'])->where('user_id', Auth::id())->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);

        //
    }
    public function pointVehicle($id)
    {
           $d = UserVehicle::find($id);
        $tot =  BookingMaster::where('vehicle_id', $id)->get()->sum('amount');
        $d['tot_point'] = $tot;
        return response()->json(['msg' => null, 'data' => $d, 'success' => true], 200);


        //
    }
    public function detailVehicle($id, Request $request)
    {
        $d = UserVehicle::find($id);
        $bm =  BookingMaster::where('vehicle_id', $id)->get();
        $ids = $bm->pluck('id');
        $tot = $bm->sum('amount');
        $d['service']  =   BookingChild::whereIn('master_id', $ids)->whereMonth('start_time', $request->month)->whereYear('start_time', $request->year)->get();
        $d['tot_point'] = $tot;
        return response()->json(['msg' => null, 'data' => $d, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required',
            'brand_id' => 'bail|required',
            'model_id' => 'bail|required',
            'fuel_id' => 'bail|required',
            'color' => 'bail|required|min:7|max:7',

            'number' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['user_id'] = Auth::id();
        UserVehicle::create($reqData);
        return response()->json(['msg' => "Vehicle Added Successfully", 'data' => null, 'success' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserVehicle  $userVehicle
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = UserVehicle::find($id);
        $data->load([
            'brand:id,name',
            'model:id,name',
            'fuel:id,name',
        ]);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserVehicle  $userVehicle
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserVehicle  $userVehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'bail|required',
            'brand_id' => 'bail|required',
            'model_id' => 'bail|required',
            'fuel_id' => 'bail|required',
            'color' => 'bail|required|min:7|max:7',

            'number' => 'bail|required',
        ]);
        $reqData = $request->all();

         $data = UserVehicle::find($id)->update($reqData);
        return response()->json(['msg' => "Vehicle UpdatedF Successfully", 'data' => null, 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserVehicle  $userVehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $data = UserVehicle::find($id)->delete();
        return response()->json(['msg' => "Vehicle Deleted Successfully", 'data' => null, 'success' => true], 200);
    }
}
