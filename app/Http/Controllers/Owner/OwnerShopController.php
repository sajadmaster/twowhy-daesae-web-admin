<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\OwnerPackage;
use App\Models\OwnerProduct;
use App\Models\OwnerService;
use App\Models\OwnerShop;
use App\Models\ShopOwner;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class OwnerShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::getDefaultDriver() == "manager") {
            $ids = (new AppHelper)->managerShop();
            $data = OwnerShop::whereIn('id',  $ids)->get();
        } else {
            // return "dert";
            $data = OwnerShop::where('owner_id', Auth::id())->get();
        }
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //     
        $request->validate([
            'name' => 'bail|required',
            'address' => 'bail|required',
            'phone_no' => 'bail|required',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required'
        ]);
        $reqData = $request->all();
        $reqData['owner_id']  = Auth::id();
        if (isset($request->image)) {
            $reqData['image'] = (new AppHelper)->saveBase64($request->image);
        }
        OwnerShop::create($reqData);
        return response()->json(['msg' => 'Shop Addedd successfully.', 'data' => null, 'success' => true], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OwnerShop  $ownerShop
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = OwnerShop::find($id);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OwnerShop  $ownerShop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OwnerShop  $ownerShop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'bail|required',
            'address' => 'bail|required',
            'phone_no' => 'bail|required',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required'
        ]);
        $reqData = $request->all();
        $reqData['owner_id']  = Auth::id();
        if (isset($request->image)) {
            $reqData['image'] = (new AppHelper)->saveBase64($request->image);
        }
        OwnerShop::find($id)->update($reqData);
        return response()->json(['msg' => 'Shop Addedd successfully.', 'data' => null, 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OwnerShop  $ownerShop
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = OwnerShop::find($id)->delete();
return response()->json(['msg' => 'Service deleted successfully.', 'data' => null, 'success' => true], 200);


        //
    }
    public function shopByRegion($id)
    {

        $ids = ShopOwner::where([['is_daesea', 1], ['status', 1]])->get('id')->pluck('id');
        $data = OwnerShop::whereIn('owner_id', $ids)->where(function (Builder $query) use ($id) {
            return $query->whereRaw('FIND_IN_SET(?, regions)', [$id]);
        })->get(['id', 'name', 'address', 'phone_no', 'image', 'start_time', 'end_time', 'owner_id']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function shopByRegionC(Request $request, $id)
    {
        $cate = $request->category;
        $ids = ShopOwner::where([['is_daesea', 1], ['status', 1]])->get('id')->pluck('id');
        $data = OwnerShop::whereIn('owner_id', $ids)->where(function (Builder $query) use ($id) {
            return $query->whereRaw('FIND_IN_SET(?, regions)', [$id]);
        })->whereRaw('FIND_IN_SET(?, categories)', [$cate])->get(['id', 'name', 'address', 'phone_no', 'image', 'start_time', 'end_time', 'owner_id']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function shopForProduct(Request $request)
    {

        $ids = ShopOwner::where([['status', 1], ['verified', 1]])->get('id')->pluck('id');
        $data = OwnerShop::whereIn('owner_id', $ids)->where(function (Builder $query) use ($request) {
            return $query->whereRaw('FIND_IN_SET(?, regions)', [$request->regions])->whereRaw('FIND_IN_SET(?, sub_categories)', [$request->sub]);
        })->get(['id', 'name', 'address', 'phone_no', 'image', 'start_time', 'end_time']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function shopService($id)
    {
        $data = OwnerShop::find($id);
        // 

        $service =  OwnerService::with([
            'prices:service_id,id,out_price,in_price,size_id',
            'prices.size' => function ($query) {
                $query->where('status', 1)->select('id', 'name', 'status');
            }
        ])->where('status', 1)->whereIn('id', $data->services)->get(['name', 'detail', 'discount', 'id']);
        return response()->json(['msg' => null, 'data' => $service, 'success' => true], 200);
    }
    public function shopProduct($id)
    {
        $data = OwnerShop::find($id);
        $data->load('productReview.images');
        $data['product'] = OwnerProduct::with(['category:id,name', 'images:id,image,product_id'])->whereIn('id', $data->products)->where('status', 1)->get(['name', 'id', 'cat_id', 'price','enable_discount','discount_price']);
        
        foreach ($data['product'] as  $value) {
            if ($value['enable_discount'] == 1) {
                $value['price'] = $value['discount_price'];
            }
        }
        
        

        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function productView($id)
    {
        $data =   OwnerProduct::with(['category:id,name', 'images:id,image,product_id'])->where([['status', 1], ['id', $id]])->first(['name', 'id', 'cat_id', 'price', 'detail','enable_discount','discount_price']);
        if ($data['enable_discount'] == 1) {
                $data['price'] = $data['discount_price'];
            }
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function getShopEmployee($id)
    {
        $data = OwnerShop::find($id);
        $e =  Employee::whereIn('id', $data->employee)->where('status', 1)->get(['id', 'name']);
        return response()->json(['msg' => null, 'data' => $e, 'success' => true], 200);
    }
    
    public function getEmployeePoint($id){
       
          $point = DB::table('booking_master')
                ->where('employee_id', $id)
                ->where('status',2)
                ->sum('amount');
                
          $count = DB::table('booking_master')
                ->where('employee_id', $id)
                ->where('status',2)
                ->count('booking_id');
          
          $month = DB::table('booking_child') ->join('booking_master', function ($join) use($id){
             $dt = \Carbon\Carbon::now()->startOfMonth()->subMonth()->format('m')+1;
             $join->on('booking_master.id', '=', 'booking_child.master_id')->where('booking_master.employee_id', $id)->where('booking_child.status',3) ->whereMonth('start_time','=', $dt)->groupBy('MONTH(`start_time`)');
        })->get();
        
          $month_count = $month -> count();
          
          if($month_count == null){
              $month_count = 0;
          }
            
        
          $m_point = DB::table('booking_child') ->join('booking_master', function ($join) use($id){
              
             $dt = \Carbon\Carbon::now()->startOfMonth()->subMonth()->format('m')+1;
             $join->on('booking_master.id', '=', 'booking_child.master_id')->where('booking_master.employee_id',$id) -> where('booking_child.status',3)->whereMonth('start_time','=', $dt)->groupBy('MONTH(`start_time`)');
          })->get()->sum('amount');
        
        
         $data3 = array(
             'point' => $point,
             'count' => $count,
             'month_count'=> $month_count,
             'month_point'=> $m_point
        );

        return response()->json(['msg' => null, 'data' => $data3, 'success' => true], 200);
    }
    
    
    
}
