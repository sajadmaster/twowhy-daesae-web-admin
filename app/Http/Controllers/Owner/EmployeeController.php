<?php


namespace App\Http\Controllers\Owner;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\BookingChild;
use App\Models\BookingMaster;
use App\Models\Employee;
use App\Models\OwnerShop;
use App\Models\ProductOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class EmployeeController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        if (Auth::getDefaultDriver() == "manager") {
            $ids = (new AppHelper)->managerShop();
            $d = OwnerShop::whereIn('id', $ids)->get()->pluck('employee');
            $merged  = array();
            foreach ($d as $a) {
                $merged = array_merge($merged, $a);
            }
            $result = array_unique($merged);
            $data =   Employee::withCount('booking')->whereIn('id', $result)->get(['name', 'image', 'id', 'email', 'id_no']);
        } else {
            $data =   Employee::withCount('booking')->where('owner_id', Auth::id())->get(['name', 'image', 'id', 'email', 'id_no']);
        }
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'email' => 'bail|required|email|unique:employee,email',
            'id_no' => 'bail|required|unique:employee,id_no',
            'name' => 'bail|required',
            'password' => 'bail|required|min:6',
            'phone_no' => 'bail|required|unique:employee,phone_no',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required'
        ]);
        $reqData = $request->all();

        if (isset($reqData['image'])) {
            $reqData['image'] = (new AppHelper)->saveBase64($reqData['image']);
        }
        $reqData['owner_id'] = Auth::id();
        Employee::create($reqData);
        return response()->json(['msg' => 'Employee added successfully', 'data' => null, 'success' => true], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $shopEmployee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = Employee::find($id)->setAppends(['imageUri', 'avgRating']);
        // $data['currency'] = AdminSetting::first()->currency_symbol;
        // $data->load(['booking.user', 'reviews']);
        return response()->json(['msg' => null, 'data' =>  $data, 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $shopEmployee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $shopEmployee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $shopEmployee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $reqData = $request->all();

        if (isset($reqData['image'])) {
            $reqData['image'] = (new AppHelper)->saveBase64($reqData['image']);
        }
        Employee::find($id)->update($reqData);
        return response()->json(['msg' => 'Employee  update successfully', 'data' => null, 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $shopEmployee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Employee::find($id)->delete();
        return response()->json(['msg' => 'Employee  delete successfully', 'data' => null, 'success' => true], 200);
    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'bail|required|email',
            'password' => 'bail|required|min:6',
        ]);
        $user = Employee::where('email', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            // if ($user['verified'] != 1) {
            //     return response()->json(['msg' => 'Please Verify your account', 'data' => null, 'success' => false, 'verification' => true], 200);
            // }
            if ($user['status'] == 0) {
                return response()->json(['msg' => 'You are block by admin', 'data' => null, 'success' => false], 200);
            }
            $token = $user->createToken('user')->accessToken;
            $user['device_token'] = $request->device_token;
            $user->save();
            $user['token'] = $token;
            return response()->json(['msg' => 'Welcome back', 'data' => $user, 'success' => true], 200);
        } else {
            return response()->json(['msg' => 'Email and Password not match with our record', 'data' => null, 'success' => false], 200);
        }
    }
    
  
    
    public function bookingApi()
    {
        $ids =  BookingMaster::where('employee_id', Auth::id())->get()->pluck('id');
        // return $ids;
        // dd($ids);
        $data = BookingChild::with(['booking:id,booking_id,address,vehicle_id', 'booking.vehicle:name,id,number', 'booking.address'])->whereIn('master_id', $ids)->whereDate('start_time', Carbon::today())->get();
        // $data =  BookingMaster::with(['vehicle:name,id,number', 'address'])->where('employee_id', Auth::id())->whereDate('start_time', Carbon::today())->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
      public function getAccumulatePoint($id){
     
          $point = DB::table('booking_master')
                ->where('employee_id', $id)
                ->where('status',2)
                ->sum('amount');
                
          $count = DB::table('booking_master')
                ->where('employee_id', $id)
                ->where('status',2)
                ->count('booking_id');
          
          $month = DB::table('booking_child') ->join('booking_master', function ($join) use($id){
             $dt = \Carbon\Carbon::now()->startOfMonth()->subMonth()->format('m')+1;
             $join->on('booking_master.id', '=', 'booking_child.master_id')->where('booking_master.employee_id', $id)->where('booking_child.status',3) ->whereMonth('start_time','=', $dt)->groupBy('MONTH(`start_time`)');
        })->get();
        
          $month_count = $month -> count();
          
          if($month_count == null){
              $month_count = 0;
          }
            
        
          $m_point = DB::table('booking_child') ->join('booking_master', function ($join) use($id){
              
             $dt = \Carbon\Carbon::now()->startOfMonth()->subMonth()->format('m')+1;
             $join->on('booking_master.id', '=', 'booking_child.master_id')->where('booking_master.employee_id',$id) -> where('booking_child.status',3)->whereMonth('start_time','=', $dt)->groupBy('MONTH(`start_time`)');
          })->get()->sum('amount');
        
        
         $data3 = array(
             'point' => $point / 10,
             'count' => $count,
             'month_count'=> $month_count,
             'month_point'=> $m_point / 10
        );

                
         return response()->json(['msg' => 'accumulatePoint', 'data'=>$data3 , 'success' => true], 200);
        
    }
    
    public function bookingApiProduct()
    {
        $data =  ProductOrder::with(['user:id,name,image,phone_no', 'item:id,name', 'review'])->where('employee_id', Auth::id())->whereDate('created_at', Carbon::today())->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function bookingBetween(Request $request)
    {
        $ids = BookingChild::whereBetween('start_time', [$request->from . " 00:00:01", $request->to . " 23:59:59"])->get()->pluck('master_id');
        $data =  BookingMaster::with(['review', 'shop:id,name,image', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item', 'employee:id,name'])->where('employee_id', Auth::id())->whereIn('id', $ids)->get();
        // ->whereBetween('start_time', [$request->from . " 00:00:01", $request->to . " 23:59:59"])
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    public function bookingBetweenComplete(Request $request)
    {
        $ids = BookingChild::whereBetween('start_time', [$request->from . " 00:00:01", $request->to . " 23:59:59"])->get()->pluck('master_id');
        $data =  BookingMaster::with(['review', 'shop:id,name,image', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item', 'employee:id,name'])->where('employee_id', Auth::id())->where('status', 3)->whereIn('id', $ids)->get();
        
        // ->whereBetween('start_time', [$request->from . " 00:00:01", $request->to . " 23:59:59"])
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    public function orderBetween(Request $request)
    {
        $data = ProductOrder::with(['user:id,name,image,phone_no', 'item:id,name', 'review'])->where('employee_id', Auth::id())->whereBetween('created_at', [$request->from . " 00:00:01", $request->to . " 23:59:59"])->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    public function orderBetweenComplete(Request $request)
    {
        $data = ProductOrder::with(['user:id,name,image,phone_no', 'item:id,name', 'review'])->where('employee_id', Auth::id())->where('status', 1)->whereBetween('created_at', [$request->from . " 00:00:01", $request->to . " 23:59:59"])->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    public function singleBooking($id)
    {
        $data =  BookingMaster::find($id);
        $data->load(['review', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item.service']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    public function bookingBetweenComplete_owner(Request $request)
    {
        $emp_id = $request->emp_id;
        $ids = BookingChild::whereBetween('start_time', [$request->from . " 00:00:01", $request->to . " 23:59:59"])->get()->pluck('master_id');
        $data =  BookingMaster::with(['review', 'shop:id,name,image', 'user:id,name,image', 'address:id,address_name,type', 'Vehicle:id,name,number', 'item', 'employee:id,name'])->where('employee_id', $emp_id)->where('status', 3)->whereIn('id', $ids)->get();
        
        // ->whereBetween('start_time', [$request->from . " 00:00:01", $request->to . " 23:59:59"])
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    
    public function orderBetweenComplete_owner(Request $request)
    {
        $emp_id = $request->emp_id;
        $data = ProductOrder::with(['user:id,name,image,phone_no', 'item:id,name', 'review'])->where('employee_id', $emp_id)->where('status', 1)->whereBetween('created_at', [$request->from . " 00:00:01", $request->to . " 23:59:59"])->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
}
