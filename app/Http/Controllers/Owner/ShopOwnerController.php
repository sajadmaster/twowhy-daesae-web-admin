<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Models\AdminSetting;
use App\Models\OwnerShop;
use App\Models\ShopOwner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ShopOwnerController extends Controller
{
    //
    public function index()
    {
        //

        $owner =   ShopOwner::all();
        return view('admin.shopOwner.index', compact('owner'));
    }
    public function changeStatus($id)
    {

        $data =   ShopOwner::findOrFail($id);
        $data->status = $data->status === 1 ? 0  : 1;
        $data->update();
        return redirect()->back()->withStatus(__('Status Is changed.'));
    }
    public function daeseaChange($id)
    {

        $data =   ShopOwner::findOrFail($id);
        $data->is_daesea = $data->is_daesea === 1 ? 0  : 1;
        $data->update();
        return redirect()->back()->withStatus(__('Status Is changed.'));
    }
    public function show(ShopOwner $id)
    {
        //
        $id->load('shop');
        $shop = $id->shop;
        return view('admin.shopOwner.shop', compact('shop'));
    }
    public function shopDetail($id)
    {
        # code...
        $data =  OwnerShop::find($id)->setAppends(['serviceData', 'employeeData']);
        $data->load(['productReview', 'bookingReview','booking.user','booking.employee', 'order.user','order.employee']);
        // return $data;
        return view('admin.shopOwner.singleShop', compact('data'));
    }

    public function store(Request $request)
    {
        //
        $request->validate([
            'email' => 'bail|required|email|unique:shop_owner,email',
            'name' => 'bail|required',
            'password' => 'bail|required|min:6',
            'phone_no' => 'bail|required|unique:shop_owner,phone_no',
        ]);
        $reqData = $request->all();

        $app = AdminSetting::get(['id', 'verification', 'sms_gateway'])->first();
        $flow =    $app->verification == 1 ? 'verification' : 'home';
        // if ($app->verification != 1) {
        //     $reqData['verified'] = 1;
        // } else {
        //     try {
        //         $res = (new TwilioController)->sendOTPUser($request, $app->sms_gateway, 'verification', 1);
        //         if ($res['success'] === true) {
        //             $reqData['otp'] = $res['otp'];
        //             // $reqData['otp'] = '0000';
        //         }
        //     } catch (\Exception $e) {
        //         $reqData['verified'] = 1;
        //         $reqData['otp'] = '0000';
        //         //  dd($e->getMessage());
        //     }
        // }
        $data = ShopOwner::create($reqData);
        // if ($app->verification != 1) {
        $token = $data->createToken('owner')->accessToken;
        $data['token'] = $token;
        // }
        return response()->json(['msg' => 'Welcome...', 'data' => $data, 'success' => true, 'flow' => $flow], 200);
    }
    public function login(Request $request)
    {
        //
        $request->validate([
            'email' => 'bail|required|email',
            'password' => 'bail|required|min:6',
        ]);
        $user = ShopOwner::where('email', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            // if ($user['verified'] != 1) {
            //     return response()->json(['msg' => 'Please Verify your account', 'data' => null, 'success' => false, 'verification' => true], 200);
            // }
            if ($user['status'] == 0) {
                return response()->json(['msg' => 'You are block by admin', 'data' => null, 'success' => false], 200);
            }
            $token = $user->createToken('owner')->accessToken;
            $user['device_token'] = $request->device_token;
            $user->save();
            $user['token'] = $token;
            return response()->json(['msg' => 'Welcome back  ', 'data' => $user, 'success' => true], 200);
        } else {
            return response()->json(['msg' => 'Email and Password not match with our record', 'data' => null, 'success' => false], 200);
        }
    }
}
