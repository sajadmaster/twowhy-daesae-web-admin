<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;

use App\Models\OwnerProduct;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;

class OwnerProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = '';
        
        if (Auth::getDefaultDriver() == "manager") {
            $data = OwnerProduct::with(['images:image,id,product_id', 'category:id,name', 'SubCategory:id,name'])->where('owner_id', Auth::user()->owner_id)->get(['id', 'name', 'detail', 'cat_id', 'sub_cat_id']);
        }else{
            $data = OwnerProduct::with(['images:image,id,product_id', 'category:id,name', 'SubCategory:id,name'])->where('owner_id', Auth::id())->get(['id', 'name', 'detail', 'cat_id', 'sub_cat_id']);
            
        }
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'bail|required',
            'detail' => 'bail|required',
            'cat_id' => 'bail|required',
            'sub_cat_id' => 'bail|required',
            'price' => 'bail|required',
            'status' => 'bail|required',
        ]);
        $reqData = $request->all();
        $reqData['owner_id'] = Auth::id();
        $data = OwnerProduct::create($reqData);
        if ($request->has('images')) {

            $images =  json_decode($request->images, true);
            foreach ($images as  $value) {
                $i = (new AppHelper)->saveBase64($value);
                ProductImage::create([
                    "product_id" => $data->id,
                    "image" => $i,
                ]);
            }
        }
       
        return response()->json(['msg' => 'Product added successfully.', 'data' => null, 'success' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OwnerProduct  $ownerProduct
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $data = OwnerProduct::find($id);
        $data->load(['images:image,id,product_id']);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OwnerProduct  $ownerProduct
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OwnerProduct  $ownerProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $data = OwnerProduct::find($id);
        $data->update($request->all());
        return response()->json(['msg' => 'Product updated successfully.', 'data' => null, 'success' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OwnerProduct  $ownerProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        ProductImage::where('product_id', $id)->delete();
        $data = OwnerProduct::find($id)->delete();
        return response()->json(['msg' => 'Product deleted successfully.', 'data' => null, 'success' => true], 200);
    }
    public function deleteImage($id)
    {
        ProductImage::find($id)->delete();
        return response()->json(['msg' => 'Product image deleted successfully.', 'data' => null, 'success' => true], 200);
    }
    public function addImage($id, Request $request)
    {
        $i = (new AppHelper)->saveBase64($request->image);

        ProductImage::create([
            "product_id" => $id,
            "image" => $i,
        ]);
        return response()->json(['msg' => 'Product image added successfully.', 'data' => null, 'success' => true], 201);
    }
}
