<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\AppHelper;
use App\Http\Controllers\Controller;
use App\Models\OwnerManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class OwnerManagerController extends Controller
{
    //
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'bail|required|email|unique:owner_manager,email',
            'name' => 'bail|required',
            'password' => 'bail|required|min:6',
            'phone_no' => 'bail|required|unique:owner_manager,phone_no',

        ]);
        $reqData = $request->all();
        $reqData['owner_id'] = Auth::id();
        if ($request->has('image')) {
            $reqData['image'] = (new AppHelper)->saveBase64($request->image);
        }
        OwnerManager::create($reqData);
        return response()->json(['msg' => 'Manager added successfully.', 'data' => null, 'success' => true], 201);
    }
    public function index()
    {

        $data = OwnerManager::where('owner_id', Auth::id())->get();
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function show($id)
    {

        $data = OwnerManager::find($id);
        return response()->json(['msg' => null, 'data' => $data, 'success' => true], 200);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'bail|sometimes',
            'password' => 'bail|sometimes|min:6',
            'phone_no' => 'bail|sometimes',
        ]);
        $data = OwnerManager::findOrFail($id);

        $reqData = $request->all();
        if ($request->has('image')) {
            $reqData['image'] = (new AppHelper)->saveBase64($request->image);
        }
        $data->update($reqData);
        return response()->json(['msg' => 'Manager updated successfully.', 'data' => null, 'success' => true], 200);
    }
    public function destroy($id)
    {
        $data = OwnerManager::find($id)->delete();
        return response()->json(['msg' => 'Manager deleted successfully.', 'data' => null, 'success' => true], 200);

        //
    }

    public function login(Request $request)
    {
        //
        $request->validate([
            'email' => 'bail|required|email',
            'password' => 'bail|required|min:6',
        ]);
        $user = OwnerManager::where('email', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            // if ($user['verified'] != 1) {
            //     return response()->json(['msg' => 'Please Verify your account', 'data' => null, 'success' => false, 'verification' => true], 200);
            // }
            if ($user['status'] == 0) {
                return response()->json(['msg' => 'You are block by admin', 'data' => null, 'success' => false], 200);
            }
            $token = $user->createToken('manger')->accessToken;
            $user['device_token'] = $request->device_token;
            $user->save();
            $user['token'] = $token;
            return response()->json(['msg' => 'Welcome back  ', 'data' => $user, 'success' => true], 200);
        } else {
            return response()->json(['msg' => 'Email and Password not match with our record', 'data' => null, 'success' => false], 200);
        }
    }
}
