<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // dd($request->header('lang'));
        if (session()->has('locale')) {
            App::setlocale(session()->get('locale'));
        } elseif ($request->header('locale') !== null) {
            App::setlocale($request->header('locale'));
        }
        return $next($request);
    }
}
