$(document).ready(function () {

    try {
        $('#summernote').summernote();

    } catch (error) {

    }
    try {
        const bookingData = JSON.parse($('#chart-orders-new').attr('data-booking'))
        const month = JSON.parse($('#chart-orders-new').attr('data-month'))
        const orderData = JSON.parse($('#chart-orders-new').attr('data-order'))
        console.log('orderData', orderData)
        var barChartData = {
            labels: month,
            datasets: [{
                    label: "Booking ",
                    backgroundColor: "rgb(194, 163, 255,0.5)",
                    borderColor: "rgb(194, 163, 255)",
                    borderWidth: 1,
                    data: bookingData
                },
                {
                    label: "Order",
                    backgroundColor: "rgb(119, 207, 207,0.5)",
                    borderColor: "rgb(119, 207, 207)",
                    borderWidth: 1,
                    data: orderData
                },

            ]
        };

        var chartOptions = {
            responsive: true,
            title: {
                display: true,
                text: "Total Booking And Order"
            },

        }

        var ctx = document.getElementById("chart-orders-new").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: "bar",
            data: barChartData,
            options: chartOptions
        });
    } catch (error) {

    }

    $('.js-example-basic').select2();
    $('#dataTable').DataTable({
        dom: 'Bfrtip',
        language: {
            paginate: {
                previous: "<i class='fas fa-angle-left'>",
                next: "<i class='fas fa-angle-right'>"
            }
        },
        buttons: [{
                extend: 'copyHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'excelHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'csvHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'pdfHtml5',
                title: new Date().toISOString()
            },
        ]
    });
    $('#dataTableProduct').DataTable({
        dom: 'Bfrtip',
        language: {
            paginate: {
                previous: "<i class='fas fa-angle-left'>",
                next: "<i class='fas fa-angle-right'>"
            }
        },
        buttons: [{
                extend: 'copyHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'excelHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'csvHtml5',
                title: new Date().toISOString()
            },
            {
                extend: 'pdfHtml5',
                title: new Date().toISOString()
            },
        ]
    });
    $(".select-tags").select2({
        tags: true
    })
    $("#users").select2();
    $("#providers").select2();

    $("#usersSelectAll").click(function () {
        $("#users > option").prop("selected", "selected");
        $("#users").trigger("change");
    });
    $("#usersSelectDeAll").click(function () {
        $("#users > option").prop("selected", '');
        $("#users").trigger("change");

    });

    $('#MaterType').change(function () {
        console.log('this.checked', this.checked)

        // if (this.checked) {
            $(".normal-price").toggle();
            $(".mega-price").toggle();
        // }
    });

});

function copyToClipboard(id) {
    var $body = document.getElementsByTagName('body')[0];
    var secretInfo = document.getElementById(id).innerHTML;
    var $tempInput = document.createElement('INPUT');
    $body.appendChild($tempInput);
    $tempInput.setAttribute('value', secretInfo)
    $tempInput.select();
    document.execCommand('copy');
    $body.removeChild($tempInput);
    alert('tag copy.')
}

function toggleInput(name) {
    var x = document.getElementsByName(name);
    var x = x[0];
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
